import { defineConfig } from 'vite';
import vitePluginStyleImport from 'vite-plugin-style-import';

export default defineConfig({
  plugins: [
    vitePluginStyleImport({
      libs: [
        {
          libraryName: 'antd',
          esModule: true,
          resolveStyle: (name) =>
            `../../../node_modules/antd/es/${name}/style/index`,
        },
      ],
    }),
  ],
  css: {
    preprocessorOptions: {
      less: {
        javascriptEnabled: true,
        // modifyVars: require('@bdpfe/theme').default,
      },
    },
  },
});
