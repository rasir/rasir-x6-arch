import React from 'react';
import { Graph, Node, Edge } from '@antv/x6';
import { ActionTypes, ArchData, ModuleConfig, NodeData, Register } from './types';
import './style/index';
export interface ArchDesignProps {
    assets?: {
        [key: string]: string;
    };
    ref?: any;
    data: ArchData;
    moduleConfig: ModuleConfig;
    autoSaveInterval?: number;
    showActions?: boolean;
    actions?: ActionTypes[];
    appendActions?: React.ReactNode;
    registerList?: Register[];
    cache?: boolean;
    defaultEdgeShape?: string;
    defaultNodeShape: string;
    moduleNodeShape?: string;
    drawerRootNode?: false | string | HTMLElement | (() => HTMLElement);
    onAddLink?: (link: Edge) => Promise<boolean>;
    onRemoveLink?: (links: Edge[]) => Promise<boolean>;
    onRemoveNode?: (nodes: Node[]) => Promise<boolean>;
    onCreateNode?: (nodeData: NodeData) => Promise<NodeData>;
    onUpdateNode?: (nodeData: NodeData) => Promise<NodeData>;
    onUndoOrRedo?: (type: 'undo' | 'redo', graph: Graph, args: {
        cmds: any[];
        options: any;
    }) => void;
    onAutoSave?: (data: ArchData) => any;
    onSave?: (data: ArchData) => Promise<boolean>;
    onChange?: (data: ArchData) => any;
}
declare const ArchDesign: React.FC<ArchDesignProps>;
export default ArchDesign;
