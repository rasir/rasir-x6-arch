const _excluded = ["moduleType", "name", "x", "y", "preId", "id", "shape", "zIndex"];

function _regeneratorRuntime() { "use strict"; /*! regenerator-runtime -- Copyright (c) 2014-present, Facebook, Inc. -- license (MIT): https://github.com/facebook/regenerator/blob/main/LICENSE */ _regeneratorRuntime = function _regeneratorRuntime() { return exports; }; var exports = {}, Op = Object.prototype, hasOwn = Op.hasOwnProperty, $Symbol = "function" == typeof Symbol ? Symbol : {}, iteratorSymbol = $Symbol.iterator || "@@iterator", asyncIteratorSymbol = $Symbol.asyncIterator || "@@asyncIterator", toStringTagSymbol = $Symbol.toStringTag || "@@toStringTag"; function define(obj, key, value) { return Object.defineProperty(obj, key, { value: value, enumerable: !0, configurable: !0, writable: !0 }), obj[key]; } try { define({}, ""); } catch (err) { define = function define(obj, key, value) { return obj[key] = value; }; } function wrap(innerFn, outerFn, self, tryLocsList) { var protoGenerator = outerFn && outerFn.prototype instanceof Generator ? outerFn : Generator, generator = Object.create(protoGenerator.prototype), context = new Context(tryLocsList || []); return generator._invoke = function (innerFn, self, context) { var state = "suspendedStart"; return function (method, arg) { if ("executing" === state) throw new Error("Generator is already running"); if ("completed" === state) { if ("throw" === method) throw arg; return doneResult(); } for (context.method = method, context.arg = arg;;) { var delegate = context.delegate; if (delegate) { var delegateResult = maybeInvokeDelegate(delegate, context); if (delegateResult) { if (delegateResult === ContinueSentinel) continue; return delegateResult; } } if ("next" === context.method) context.sent = context._sent = context.arg;else if ("throw" === context.method) { if ("suspendedStart" === state) throw state = "completed", context.arg; context.dispatchException(context.arg); } else "return" === context.method && context.abrupt("return", context.arg); state = "executing"; var record = tryCatch(innerFn, self, context); if ("normal" === record.type) { if (state = context.done ? "completed" : "suspendedYield", record.arg === ContinueSentinel) continue; return { value: record.arg, done: context.done }; } "throw" === record.type && (state = "completed", context.method = "throw", context.arg = record.arg); } }; }(innerFn, self, context), generator; } function tryCatch(fn, obj, arg) { try { return { type: "normal", arg: fn.call(obj, arg) }; } catch (err) { return { type: "throw", arg: err }; } } exports.wrap = wrap; var ContinueSentinel = {}; function Generator() {} function GeneratorFunction() {} function GeneratorFunctionPrototype() {} var IteratorPrototype = {}; define(IteratorPrototype, iteratorSymbol, function () { return this; }); var getProto = Object.getPrototypeOf, NativeIteratorPrototype = getProto && getProto(getProto(values([]))); NativeIteratorPrototype && NativeIteratorPrototype !== Op && hasOwn.call(NativeIteratorPrototype, iteratorSymbol) && (IteratorPrototype = NativeIteratorPrototype); var Gp = GeneratorFunctionPrototype.prototype = Generator.prototype = Object.create(IteratorPrototype); function defineIteratorMethods(prototype) { ["next", "throw", "return"].forEach(function (method) { define(prototype, method, function (arg) { return this._invoke(method, arg); }); }); } function AsyncIterator(generator, PromiseImpl) { function invoke(method, arg, resolve, reject) { var record = tryCatch(generator[method], generator, arg); if ("throw" !== record.type) { var result = record.arg, value = result.value; return value && "object" == typeof value && hasOwn.call(value, "__await") ? PromiseImpl.resolve(value.__await).then(function (value) { invoke("next", value, resolve, reject); }, function (err) { invoke("throw", err, resolve, reject); }) : PromiseImpl.resolve(value).then(function (unwrapped) { result.value = unwrapped, resolve(result); }, function (error) { return invoke("throw", error, resolve, reject); }); } reject(record.arg); } var previousPromise; this._invoke = function (method, arg) { function callInvokeWithMethodAndArg() { return new PromiseImpl(function (resolve, reject) { invoke(method, arg, resolve, reject); }); } return previousPromise = previousPromise ? previousPromise.then(callInvokeWithMethodAndArg, callInvokeWithMethodAndArg) : callInvokeWithMethodAndArg(); }; } function maybeInvokeDelegate(delegate, context) { var method = delegate.iterator[context.method]; if (undefined === method) { if (context.delegate = null, "throw" === context.method) { if (delegate.iterator.return && (context.method = "return", context.arg = undefined, maybeInvokeDelegate(delegate, context), "throw" === context.method)) return ContinueSentinel; context.method = "throw", context.arg = new TypeError("The iterator does not provide a 'throw' method"); } return ContinueSentinel; } var record = tryCatch(method, delegate.iterator, context.arg); if ("throw" === record.type) return context.method = "throw", context.arg = record.arg, context.delegate = null, ContinueSentinel; var info = record.arg; return info ? info.done ? (context[delegate.resultName] = info.value, context.next = delegate.nextLoc, "return" !== context.method && (context.method = "next", context.arg = undefined), context.delegate = null, ContinueSentinel) : info : (context.method = "throw", context.arg = new TypeError("iterator result is not an object"), context.delegate = null, ContinueSentinel); } function pushTryEntry(locs) { var entry = { tryLoc: locs[0] }; 1 in locs && (entry.catchLoc = locs[1]), 2 in locs && (entry.finallyLoc = locs[2], entry.afterLoc = locs[3]), this.tryEntries.push(entry); } function resetTryEntry(entry) { var record = entry.completion || {}; record.type = "normal", delete record.arg, entry.completion = record; } function Context(tryLocsList) { this.tryEntries = [{ tryLoc: "root" }], tryLocsList.forEach(pushTryEntry, this), this.reset(!0); } function values(iterable) { if (iterable) { var iteratorMethod = iterable[iteratorSymbol]; if (iteratorMethod) return iteratorMethod.call(iterable); if ("function" == typeof iterable.next) return iterable; if (!isNaN(iterable.length)) { var i = -1, next = function next() { for (; ++i < iterable.length;) if (hasOwn.call(iterable, i)) return next.value = iterable[i], next.done = !1, next; return next.value = undefined, next.done = !0, next; }; return next.next = next; } } return { next: doneResult }; } function doneResult() { return { value: undefined, done: !0 }; } return GeneratorFunction.prototype = GeneratorFunctionPrototype, define(Gp, "constructor", GeneratorFunctionPrototype), define(GeneratorFunctionPrototype, "constructor", GeneratorFunction), GeneratorFunction.displayName = define(GeneratorFunctionPrototype, toStringTagSymbol, "GeneratorFunction"), exports.isGeneratorFunction = function (genFun) { var ctor = "function" == typeof genFun && genFun.constructor; return !!ctor && (ctor === GeneratorFunction || "GeneratorFunction" === (ctor.displayName || ctor.name)); }, exports.mark = function (genFun) { return Object.setPrototypeOf ? Object.setPrototypeOf(genFun, GeneratorFunctionPrototype) : (genFun.__proto__ = GeneratorFunctionPrototype, define(genFun, toStringTagSymbol, "GeneratorFunction")), genFun.prototype = Object.create(Gp), genFun; }, exports.awrap = function (arg) { return { __await: arg }; }, defineIteratorMethods(AsyncIterator.prototype), define(AsyncIterator.prototype, asyncIteratorSymbol, function () { return this; }), exports.AsyncIterator = AsyncIterator, exports.async = function (innerFn, outerFn, self, tryLocsList, PromiseImpl) { void 0 === PromiseImpl && (PromiseImpl = Promise); var iter = new AsyncIterator(wrap(innerFn, outerFn, self, tryLocsList), PromiseImpl); return exports.isGeneratorFunction(outerFn) ? iter : iter.next().then(function (result) { return result.done ? result.value : iter.next(); }); }, defineIteratorMethods(Gp), define(Gp, toStringTagSymbol, "Generator"), define(Gp, iteratorSymbol, function () { return this; }), define(Gp, "toString", function () { return "[object Generator]"; }), exports.keys = function (object) { var keys = []; for (var key in object) keys.push(key); return keys.reverse(), function next() { for (; keys.length;) { var key = keys.pop(); if (key in object) return next.value = key, next.done = !1, next; } return next.done = !0, next; }; }, exports.values = values, Context.prototype = { constructor: Context, reset: function reset(skipTempReset) { if (this.prev = 0, this.next = 0, this.sent = this._sent = undefined, this.done = !1, this.delegate = null, this.method = "next", this.arg = undefined, this.tryEntries.forEach(resetTryEntry), !skipTempReset) for (var name in this) "t" === name.charAt(0) && hasOwn.call(this, name) && !isNaN(+name.slice(1)) && (this[name] = undefined); }, stop: function stop() { this.done = !0; var rootRecord = this.tryEntries[0].completion; if ("throw" === rootRecord.type) throw rootRecord.arg; return this.rval; }, dispatchException: function dispatchException(exception) { if (this.done) throw exception; var context = this; function handle(loc, caught) { return record.type = "throw", record.arg = exception, context.next = loc, caught && (context.method = "next", context.arg = undefined), !!caught; } for (var i = this.tryEntries.length - 1; i >= 0; --i) { var entry = this.tryEntries[i], record = entry.completion; if ("root" === entry.tryLoc) return handle("end"); if (entry.tryLoc <= this.prev) { var hasCatch = hasOwn.call(entry, "catchLoc"), hasFinally = hasOwn.call(entry, "finallyLoc"); if (hasCatch && hasFinally) { if (this.prev < entry.catchLoc) return handle(entry.catchLoc, !0); if (this.prev < entry.finallyLoc) return handle(entry.finallyLoc); } else if (hasCatch) { if (this.prev < entry.catchLoc) return handle(entry.catchLoc, !0); } else { if (!hasFinally) throw new Error("try statement without catch or finally"); if (this.prev < entry.finallyLoc) return handle(entry.finallyLoc); } } } }, abrupt: function abrupt(type, arg) { for (var i = this.tryEntries.length - 1; i >= 0; --i) { var entry = this.tryEntries[i]; if (entry.tryLoc <= this.prev && hasOwn.call(entry, "finallyLoc") && this.prev < entry.finallyLoc) { var finallyEntry = entry; break; } } finallyEntry && ("break" === type || "continue" === type) && finallyEntry.tryLoc <= arg && arg <= finallyEntry.finallyLoc && (finallyEntry = null); var record = finallyEntry ? finallyEntry.completion : {}; return record.type = type, record.arg = arg, finallyEntry ? (this.method = "next", this.next = finallyEntry.finallyLoc, ContinueSentinel) : this.complete(record); }, complete: function complete(record, afterLoc) { if ("throw" === record.type) throw record.arg; return "break" === record.type || "continue" === record.type ? this.next = record.arg : "return" === record.type ? (this.rval = this.arg = record.arg, this.method = "return", this.next = "end") : "normal" === record.type && afterLoc && (this.next = afterLoc), ContinueSentinel; }, finish: function finish(finallyLoc) { for (var i = this.tryEntries.length - 1; i >= 0; --i) { var entry = this.tryEntries[i]; if (entry.finallyLoc === finallyLoc) return this.complete(entry.completion, entry.afterLoc), resetTryEntry(entry), ContinueSentinel; } }, catch: function _catch(tryLoc) { for (var i = this.tryEntries.length - 1; i >= 0; --i) { var entry = this.tryEntries[i]; if (entry.tryLoc === tryLoc) { var record = entry.completion; if ("throw" === record.type) { var thrown = record.arg; resetTryEntry(entry); } return thrown; } } throw new Error("illegal catch attempt"); }, delegateYield: function delegateYield(iterable, resultName, nextLoc) { return this.delegate = { iterator: values(iterable), resultName: resultName, nextLoc: nextLoc }, "next" === this.method && (this.arg = undefined), ContinueSentinel; } }, exports; }

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); enumerableOnly && (symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; })), keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = null != arguments[i] ? arguments[i] : {}; i % 2 ? ownKeys(Object(source), !0).forEach(function (key) { _defineProperty(target, key, source[key]); }) : Object.getOwnPropertyDescriptors ? Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)) : ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function _objectWithoutProperties(source, excluded) { if (source == null) return {}; var target = _objectWithoutPropertiesLoose(source, excluded); var key, i; if (Object.getOwnPropertySymbols) { var sourceSymbolKeys = Object.getOwnPropertySymbols(source); for (i = 0; i < sourceSymbolKeys.length; i++) { key = sourceSymbolKeys[i]; if (excluded.indexOf(key) >= 0) continue; if (!Object.prototype.propertyIsEnumerable.call(source, key)) continue; target[key] = source[key]; } } return target; }

function _objectWithoutPropertiesLoose(source, excluded) { if (source == null) return {}; var target = {}; var sourceKeys = Object.keys(source); var key, i; for (i = 0; i < sourceKeys.length; i++) { key = sourceKeys[i]; if (excluded.indexOf(key) >= 0) continue; target[key] = source[key]; } return target; }

function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

import defaultData from './defaultData';
import IndexdbHelper from './components/IndexdbHelper';
import { getNodeAttr } from './components/Lane/tools';
export const defaultModuleNodeName = 'moduleNode';
export const defaultEdgeShapeName = 'laneEdge';
export const dbHelper = new IndexdbHelper('arch_desgin', [{
  name: 'graphDatas',
  keyPath: 'id'
}]); // // 自动保存数据

let autoSaveTimer;
export function autoSaveJson(_x, _x2, _x3) {
  return _autoSaveJson.apply(this, arguments);
} // 甬道样式

function _autoSaveJson() {
  _autoSaveJson = _asyncToGenerator( /*#__PURE__*/_regeneratorRuntime().mark(function _callee2(graphData, graph, onAutoSave) {
    var cache;
    return _regeneratorRuntime().wrap(function _callee2$(_context2) {
      while (1) switch (_context2.prev = _context2.next) {
        case 0:
          if (autoSaveTimer) clearTimeout(autoSaveTimer);
          cache = window._useCache;

          if (!(!onAutoSave && !cache)) {
            _context2.next = 4;
            break;
          }

          return _context2.abrupt("return");

        case 4:
          return _context2.abrupt("return", new Promise(resolve => {
            autoSaveTimer = setTimeout( /*#__PURE__*/_asyncToGenerator( /*#__PURE__*/_regeneratorRuntime().mark(function _callee() {
              var data, isExist;
              return _regeneratorRuntime().wrap(function _callee$(_context) {
                while (1) switch (_context.prev = _context.next) {
                  case 0:
                    data = {
                      id: '1',
                      updateTime: Date.now(),
                      data: undefined,
                      expired: window._saveInterval + 10000
                    }; // 如果有 graphData 就强制保存 graphData

                    if (graphData) {
                      data.data = graphData;
                    } else if (graph) {
                      // 如果没有 graphData 有graph 就保存 graph 中的数据
                      data.data = getGraphJSONData(graph);
                    }

                    onAutoSave && onAutoSave(data.data); // 如果都没有 data 为 undefined 将在下次恢复时直接使用 传入的数据

                    if (cache) {
                      _context.next = 5;
                      break;
                    }

                    return _context.abrupt("return");

                  case 5:
                    _context.next = 7;
                    return dbHelper.ready();

                  case 7:
                    _context.next = 9;
                    return new Promise(resolve => setTimeout(resolve, 100));

                  case 9:
                    _context.next = 11;
                    return dbHelper.findByPk('graphDatas', '1');

                  case 11:
                    isExist = !!_context.sent;

                    if (!isExist) {
                      _context.next = 17;
                      break;
                    }

                    _context.next = 15;
                    return dbHelper.update('graphDatas', data);

                  case 15:
                    _context.next = 19;
                    break;

                  case 17:
                    _context.next = 19;
                    return dbHelper.create('graphDatas', data);

                  case 19:
                    resolve(1);

                  case 20:
                  case "end":
                    return _context.stop();
                }
              }, _callee);
            })), 100);
          }));

        case 5:
        case "end":
          return _context2.stop();
      }
    }, _callee2);
  }));
  return _autoSaveJson.apply(this, arguments);
}

export function getLaneStyle(index) {
  const nameRectFill = ['#EAFEEC', '#F9F7E6', '#E0EEFA', '#DFE5E6'];
  const nameTextFill = ['#6A8E67', '#978A64', '#495A7F', '#859092'];
  const bottomBorderStroke = ['#9BD787', '#E1BD81', '#7BA3FC', '#859092'];
  return {
    nameRect: {
      fill: nameRectFill[index]
    },
    nameText: {
      fill: nameTextFill[index]
    },
    bottomBorder: {
      stroke: bottomBorderStroke[index]
    }
  };
} // 将原数据转换成画布数据

export function getGraphData(dataSource, defaultEdgeShape) {
  if (!dataSource) return defaultData;
  const _dataSource$lanes = dataSource.lanes,
        lanes = _dataSource$lanes === void 0 ? [] : _dataSource$lanes,
        _dataSource$nodes = dataSource.nodes,
        nodes = _dataSource$nodes === void 0 ? [] : _dataSource$nodes,
        _dataSource$links = dataSource.links,
        links = _dataSource$links === void 0 ? [] : _dataSource$links;
  let lh = 25;
  let maxWidth = 0;
  const laneList = lanes.map((data, i) => {
    const id = data.id,
          height = data.height,
          x = data.x,
          y = data.y,
          name = data.name,
          width = data.width,
          attrs = data.attrs;
    lh += height;
    data.type = 'lane';
    data.sort = i;
    maxWidth = Math.max(maxWidth, width || 0);
    const laneData = {
      id,
      shape: 'lane',
      data,
      height,
      label: (name || '').split('').join('\n'),
      position: {
        x: x || 0,
        y: y || lh - height
      },
      attrs: attrs || getLaneStyle(i % 4),
      type: 'lane'
    };
    return laneData;
  });
  const nodeList = nodes.map(node => createGraphNodeData(node));
  maxWidth = Math.max(maxWidth, ...nodeList.map(node => (node.position.x || 0) + 200));
  const linkList = links.map(link => {
    const id = link.id,
          source = link.source,
          target = link.target,
          shape = link.shape;
    link.type = 'link';
    return {
      id: id || `${source}_${target}`,
      shape: shape || defaultEdgeShape,
      source,
      target,
      data: link,
      type: 'link'
    };
  });
  return {
    width: maxWidth,
    height: lh + 25,
    laneList,
    nodeList,
    linkList
  };
} // 两行显示名称

/**
 * 用于实例详情图中任务名称显示
 * @param {string} label 需要处理的字符
 * @param {number} maxTextLen 最大长度 默认20
 * @param {number} maxLine  最大行数 默认1
 * @param {string} placehoder 中间占位字符 默认...
 */

export function getLabelText({
  label = '',
  placehoder = '...',
  maxTextLen = 20,
  maxLine = 2
}) {
  if (!label) return '';
  const textList = [];
  let len = 0;
  let lineCount = 0;
  (label || '').split('').every(char => {
    let crrLen;
    if (len === 0) textList[textList.length] = [];
    if (/[ilI]/.test(char)) crrLen = 0.5;else if (/[A-Z]/.test(char)) crrLen = 1.5;else if (/[^\u0000-\u00FF]/.test(char)) crrLen = 2;else crrLen = 1;

    if (lineCount === maxLine - 1 && crrLen + len >= maxTextLen - placehoder.length) {
      textList[textList.length - 1].push(placehoder);
      return false;
    }

    if (crrLen + len < maxTextLen + 1) {
      textList[textList.length - 1].push(char);
      len += crrLen;
    } else if (lineCount < maxLine) {
      textList[textList.length] = [char];
      len = crrLen;
      lineCount += 1;
    }

    return true;
  });
  return textList.map(list => list.join('')).join('\n');
}
export function nextTick(callback) {
  return Promise.resolve().then(callback);
}
export function getDndConfig(target) {
  return {
    target,
    // delegateGraphOptions
    validateNode: () => false,
    animation: true,
    getDragNode: () => {},
    getDropNode: () => {}
  };
} // 将配置数据转换成节点所需数据

export function createGraphNodeData(nodeData, config) {
  const _ref = config || {},
        graph = _ref.graph,
        assets = _ref.assets;

  const moduleType = nodeData.moduleType,
        name = nodeData.name,
        x = nodeData.x,
        y = nodeData.y,
        preId = nodeData.preId,
        nodeId = nodeData.id,
        shape = nodeData.shape,
        zIndex = nodeData.zIndex,
        rest = _objectWithoutProperties(nodeData, _excluded);

  let id = nodeId;

  if (!id) {
    id = `${Date.now().toString(32)}_${Math.floor(Math.random() * 100000).toString(32)}`;
  }

  let position = {
    x: 0,
    y: 0
  };
  let attrs = {};

  if (x !== undefined && y !== undefined) {
    position = {
      x,
      y
    };
  } else if (graph && preId) {
    const lane = graph.getCellById(preId);

    const _lane$position = lane.position(),
          x = _lane$position.x,
          y = _lane$position.y;

    position = {
      x: x + 60,
      y: y + 10
    };
  }

  if (assets) {
    attrs = getNodeAttr(shape, nodeData, assets);
  }

  return {
    id,
    shape,
    position,
    parent: preId,
    type: 'node',
    zIndex: zIndex || 2,
    data: _objectSpread(_objectSpread({
      moduleType,
      name,
      preId,
      id
    }, rest), {}, {
      type: 'node'
    }),
    attrs
  };
} // 将画布中的所有元素转换成 后端保存数据

export function getGraphJSONData(graph) {
  const lanes = [];
  const nodes = [];
  const links = [];

  if (!graph) {
    return {
      lanes,
      nodes,
      links
    };
  }

  const _graph$toJSON = graph.toJSON(),
        cells = _graph$toJSON.cells;

  const _graph$container = graph.container,
        clientWidth = _graph$container.clientWidth,
        clientHeight = _graph$container.clientHeight;
  cells.forEach(cell => {
    const shape = cell.shape,
          position = cell.position,
          size = cell.size,
          data = cell.data,
          parent = cell.parent,
          zIndex = cell.zIndex,
          source = cell.source,
          target = cell.target,
          cellType = cell.type;
    const type = cellType || (data === null || data === void 0 ? void 0 : data.type);
    if (!type) return;

    if (type === 'link') {
      links.push({
        source: source.cell,
        target: target.cell,
        zIndex: zIndex || 2,
        shape,
        type
      });
    } else {
      const node = _objectSpread(_objectSpread(_objectSpread(_objectSpread({}, data), size), position), {}, {
        preId: parent,
        type,
        shape
      });

      if (type === 'lane') {
        node.zIndex = zIndex || 1;
        lanes.push(node);
      } else {
        node.zIndex = zIndex || 2;
        nodes.push(node);
      }
    }
  });
  return {
    width: clientWidth,
    height: clientHeight,
    lanes,
    nodes,
    links
  };
}
export function launchFullScreen(element) {
  if (element.requestFullscreen) {
    element.requestFullscreen();
  } else if (element.mozRequestFullScreen) {
    element.mozRequestFullScreen();
  } else if (element.webkitRequestFullscreen) {
    element.webkitRequestFullscreen();
  } else if (element.msRequestFullscreen) {
    element.msRequestFullscreen();
  }
}
export function exitFullscreen() {
  if (document.exitFullscreen) {
    document.exitFullscreen();
  } else if (document.mozCancelFullScreen) {
    document.mozCancelFullScreen();
  } else if (document.webkitExitFullscreen) {
    document.webkitExitFullscreen();
  }
}
export function getKey() {
  return Date.now().toString(32) + '_' + (Math.random() * 10000).toFixed(0);
}