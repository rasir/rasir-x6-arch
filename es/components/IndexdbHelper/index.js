const _excluded = ["keyPath", "indexs"],
      _excluded2 = ["name"];

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); enumerableOnly && (symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; })), keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = null != arguments[i] ? arguments[i] : {}; i % 2 ? ownKeys(Object(source), !0).forEach(function (key) { _defineProperty(target, key, source[key]); }) : Object.getOwnPropertyDescriptors ? Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)) : ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } return target; }

function _objectWithoutProperties(source, excluded) { if (source == null) return {}; var target = _objectWithoutPropertiesLoose(source, excluded); var key, i; if (Object.getOwnPropertySymbols) { var sourceSymbolKeys = Object.getOwnPropertySymbols(source); for (i = 0; i < sourceSymbolKeys.length; i++) { key = sourceSymbolKeys[i]; if (excluded.indexOf(key) >= 0) continue; if (!Object.prototype.propertyIsEnumerable.call(source, key)) continue; target[key] = source[key]; } } return target; }

function _objectWithoutPropertiesLoose(source, excluded) { if (source == null) return {}; var target = {}; var sourceKeys = Object.keys(source); var key, i; for (i = 0; i < sourceKeys.length; i++) { key = sourceKeys[i]; if (excluded.indexOf(key) >= 0) continue; target[key] = source[key]; } return target; }

function _regeneratorRuntime() { "use strict"; /*! regenerator-runtime -- Copyright (c) 2014-present, Facebook, Inc. -- license (MIT): https://github.com/facebook/regenerator/blob/main/LICENSE */ _regeneratorRuntime = function _regeneratorRuntime() { return exports; }; var exports = {}, Op = Object.prototype, hasOwn = Op.hasOwnProperty, $Symbol = "function" == typeof Symbol ? Symbol : {}, iteratorSymbol = $Symbol.iterator || "@@iterator", asyncIteratorSymbol = $Symbol.asyncIterator || "@@asyncIterator", toStringTagSymbol = $Symbol.toStringTag || "@@toStringTag"; function define(obj, key, value) { return Object.defineProperty(obj, key, { value: value, enumerable: !0, configurable: !0, writable: !0 }), obj[key]; } try { define({}, ""); } catch (err) { define = function define(obj, key, value) { return obj[key] = value; }; } function wrap(innerFn, outerFn, self, tryLocsList) { var protoGenerator = outerFn && outerFn.prototype instanceof Generator ? outerFn : Generator, generator = Object.create(protoGenerator.prototype), context = new Context(tryLocsList || []); return generator._invoke = function (innerFn, self, context) { var state = "suspendedStart"; return function (method, arg) { if ("executing" === state) throw new Error("Generator is already running"); if ("completed" === state) { if ("throw" === method) throw arg; return doneResult(); } for (context.method = method, context.arg = arg;;) { var delegate = context.delegate; if (delegate) { var delegateResult = maybeInvokeDelegate(delegate, context); if (delegateResult) { if (delegateResult === ContinueSentinel) continue; return delegateResult; } } if ("next" === context.method) context.sent = context._sent = context.arg;else if ("throw" === context.method) { if ("suspendedStart" === state) throw state = "completed", context.arg; context.dispatchException(context.arg); } else "return" === context.method && context.abrupt("return", context.arg); state = "executing"; var record = tryCatch(innerFn, self, context); if ("normal" === record.type) { if (state = context.done ? "completed" : "suspendedYield", record.arg === ContinueSentinel) continue; return { value: record.arg, done: context.done }; } "throw" === record.type && (state = "completed", context.method = "throw", context.arg = record.arg); } }; }(innerFn, self, context), generator; } function tryCatch(fn, obj, arg) { try { return { type: "normal", arg: fn.call(obj, arg) }; } catch (err) { return { type: "throw", arg: err }; } } exports.wrap = wrap; var ContinueSentinel = {}; function Generator() {} function GeneratorFunction() {} function GeneratorFunctionPrototype() {} var IteratorPrototype = {}; define(IteratorPrototype, iteratorSymbol, function () { return this; }); var getProto = Object.getPrototypeOf, NativeIteratorPrototype = getProto && getProto(getProto(values([]))); NativeIteratorPrototype && NativeIteratorPrototype !== Op && hasOwn.call(NativeIteratorPrototype, iteratorSymbol) && (IteratorPrototype = NativeIteratorPrototype); var Gp = GeneratorFunctionPrototype.prototype = Generator.prototype = Object.create(IteratorPrototype); function defineIteratorMethods(prototype) { ["next", "throw", "return"].forEach(function (method) { define(prototype, method, function (arg) { return this._invoke(method, arg); }); }); } function AsyncIterator(generator, PromiseImpl) { function invoke(method, arg, resolve, reject) { var record = tryCatch(generator[method], generator, arg); if ("throw" !== record.type) { var result = record.arg, value = result.value; return value && "object" == typeof value && hasOwn.call(value, "__await") ? PromiseImpl.resolve(value.__await).then(function (value) { invoke("next", value, resolve, reject); }, function (err) { invoke("throw", err, resolve, reject); }) : PromiseImpl.resolve(value).then(function (unwrapped) { result.value = unwrapped, resolve(result); }, function (error) { return invoke("throw", error, resolve, reject); }); } reject(record.arg); } var previousPromise; this._invoke = function (method, arg) { function callInvokeWithMethodAndArg() { return new PromiseImpl(function (resolve, reject) { invoke(method, arg, resolve, reject); }); } return previousPromise = previousPromise ? previousPromise.then(callInvokeWithMethodAndArg, callInvokeWithMethodAndArg) : callInvokeWithMethodAndArg(); }; } function maybeInvokeDelegate(delegate, context) { var method = delegate.iterator[context.method]; if (undefined === method) { if (context.delegate = null, "throw" === context.method) { if (delegate.iterator.return && (context.method = "return", context.arg = undefined, maybeInvokeDelegate(delegate, context), "throw" === context.method)) return ContinueSentinel; context.method = "throw", context.arg = new TypeError("The iterator does not provide a 'throw' method"); } return ContinueSentinel; } var record = tryCatch(method, delegate.iterator, context.arg); if ("throw" === record.type) return context.method = "throw", context.arg = record.arg, context.delegate = null, ContinueSentinel; var info = record.arg; return info ? info.done ? (context[delegate.resultName] = info.value, context.next = delegate.nextLoc, "return" !== context.method && (context.method = "next", context.arg = undefined), context.delegate = null, ContinueSentinel) : info : (context.method = "throw", context.arg = new TypeError("iterator result is not an object"), context.delegate = null, ContinueSentinel); } function pushTryEntry(locs) { var entry = { tryLoc: locs[0] }; 1 in locs && (entry.catchLoc = locs[1]), 2 in locs && (entry.finallyLoc = locs[2], entry.afterLoc = locs[3]), this.tryEntries.push(entry); } function resetTryEntry(entry) { var record = entry.completion || {}; record.type = "normal", delete record.arg, entry.completion = record; } function Context(tryLocsList) { this.tryEntries = [{ tryLoc: "root" }], tryLocsList.forEach(pushTryEntry, this), this.reset(!0); } function values(iterable) { if (iterable) { var iteratorMethod = iterable[iteratorSymbol]; if (iteratorMethod) return iteratorMethod.call(iterable); if ("function" == typeof iterable.next) return iterable; if (!isNaN(iterable.length)) { var i = -1, next = function next() { for (; ++i < iterable.length;) if (hasOwn.call(iterable, i)) return next.value = iterable[i], next.done = !1, next; return next.value = undefined, next.done = !0, next; }; return next.next = next; } } return { next: doneResult }; } function doneResult() { return { value: undefined, done: !0 }; } return GeneratorFunction.prototype = GeneratorFunctionPrototype, define(Gp, "constructor", GeneratorFunctionPrototype), define(GeneratorFunctionPrototype, "constructor", GeneratorFunction), GeneratorFunction.displayName = define(GeneratorFunctionPrototype, toStringTagSymbol, "GeneratorFunction"), exports.isGeneratorFunction = function (genFun) { var ctor = "function" == typeof genFun && genFun.constructor; return !!ctor && (ctor === GeneratorFunction || "GeneratorFunction" === (ctor.displayName || ctor.name)); }, exports.mark = function (genFun) { return Object.setPrototypeOf ? Object.setPrototypeOf(genFun, GeneratorFunctionPrototype) : (genFun.__proto__ = GeneratorFunctionPrototype, define(genFun, toStringTagSymbol, "GeneratorFunction")), genFun.prototype = Object.create(Gp), genFun; }, exports.awrap = function (arg) { return { __await: arg }; }, defineIteratorMethods(AsyncIterator.prototype), define(AsyncIterator.prototype, asyncIteratorSymbol, function () { return this; }), exports.AsyncIterator = AsyncIterator, exports.async = function (innerFn, outerFn, self, tryLocsList, PromiseImpl) { void 0 === PromiseImpl && (PromiseImpl = Promise); var iter = new AsyncIterator(wrap(innerFn, outerFn, self, tryLocsList), PromiseImpl); return exports.isGeneratorFunction(outerFn) ? iter : iter.next().then(function (result) { return result.done ? result.value : iter.next(); }); }, defineIteratorMethods(Gp), define(Gp, toStringTagSymbol, "Generator"), define(Gp, iteratorSymbol, function () { return this; }), define(Gp, "toString", function () { return "[object Generator]"; }), exports.keys = function (object) { var keys = []; for (var key in object) keys.push(key); return keys.reverse(), function next() { for (; keys.length;) { var key = keys.pop(); if (key in object) return next.value = key, next.done = !1, next; } return next.done = !0, next; }; }, exports.values = values, Context.prototype = { constructor: Context, reset: function reset(skipTempReset) { if (this.prev = 0, this.next = 0, this.sent = this._sent = undefined, this.done = !1, this.delegate = null, this.method = "next", this.arg = undefined, this.tryEntries.forEach(resetTryEntry), !skipTempReset) for (var name in this) "t" === name.charAt(0) && hasOwn.call(this, name) && !isNaN(+name.slice(1)) && (this[name] = undefined); }, stop: function stop() { this.done = !0; var rootRecord = this.tryEntries[0].completion; if ("throw" === rootRecord.type) throw rootRecord.arg; return this.rval; }, dispatchException: function dispatchException(exception) { if (this.done) throw exception; var context = this; function handle(loc, caught) { return record.type = "throw", record.arg = exception, context.next = loc, caught && (context.method = "next", context.arg = undefined), !!caught; } for (var i = this.tryEntries.length - 1; i >= 0; --i) { var entry = this.tryEntries[i], record = entry.completion; if ("root" === entry.tryLoc) return handle("end"); if (entry.tryLoc <= this.prev) { var hasCatch = hasOwn.call(entry, "catchLoc"), hasFinally = hasOwn.call(entry, "finallyLoc"); if (hasCatch && hasFinally) { if (this.prev < entry.catchLoc) return handle(entry.catchLoc, !0); if (this.prev < entry.finallyLoc) return handle(entry.finallyLoc); } else if (hasCatch) { if (this.prev < entry.catchLoc) return handle(entry.catchLoc, !0); } else { if (!hasFinally) throw new Error("try statement without catch or finally"); if (this.prev < entry.finallyLoc) return handle(entry.finallyLoc); } } } }, abrupt: function abrupt(type, arg) { for (var i = this.tryEntries.length - 1; i >= 0; --i) { var entry = this.tryEntries[i]; if (entry.tryLoc <= this.prev && hasOwn.call(entry, "finallyLoc") && this.prev < entry.finallyLoc) { var finallyEntry = entry; break; } } finallyEntry && ("break" === type || "continue" === type) && finallyEntry.tryLoc <= arg && arg <= finallyEntry.finallyLoc && (finallyEntry = null); var record = finallyEntry ? finallyEntry.completion : {}; return record.type = type, record.arg = arg, finallyEntry ? (this.method = "next", this.next = finallyEntry.finallyLoc, ContinueSentinel) : this.complete(record); }, complete: function complete(record, afterLoc) { if ("throw" === record.type) throw record.arg; return "break" === record.type || "continue" === record.type ? this.next = record.arg : "return" === record.type ? (this.rval = this.arg = record.arg, this.method = "return", this.next = "end") : "normal" === record.type && afterLoc && (this.next = afterLoc), ContinueSentinel; }, finish: function finish(finallyLoc) { for (var i = this.tryEntries.length - 1; i >= 0; --i) { var entry = this.tryEntries[i]; if (entry.finallyLoc === finallyLoc) return this.complete(entry.completion, entry.afterLoc), resetTryEntry(entry), ContinueSentinel; } }, catch: function _catch(tryLoc) { for (var i = this.tryEntries.length - 1; i >= 0; --i) { var entry = this.tryEntries[i]; if (entry.tryLoc === tryLoc) { var record = entry.completion; if ("throw" === record.type) { var thrown = record.arg; resetTryEntry(entry); } return thrown; } } throw new Error("illegal catch attempt"); }, delegateYield: function delegateYield(iterable, resultName, nextLoc) { return this.delegate = { iterator: values(iterable), resultName: resultName, nextLoc: nextLoc }, "next" === this.method && (this.arg = undefined), ContinueSentinel; } }, exports; }

function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

class IndexdbHelper {
  constructor(databaseName, tbConfigs, dbVersion) {
    _defineProperty(this, "haveIndex", false);

    _defineProperty(this, "canuseIndex", false);

    _defineProperty(this, "tbConfigs", []);

    this.databaseName = databaseName;
    this.dbVersion = dbVersion || 2;
    this.tbConfigs = tbConfigs || [];

    if (window.indexedDB) {
      this.haveIndex = true;
    } else this.haveIndex = false;
  } // indexdb 初始化


  ready() {
    var _this = this;

    if (this.haveIndex) {
      if (this.db) return Promise.resolve(this.db);
      return new Promise( /*#__PURE__*/function () {
        var _ref = _asyncToGenerator( /*#__PURE__*/_regeneratorRuntime().mark(function _callee2(resolve, reject) {
          var request;
          return _regeneratorRuntime().wrap(function _callee2$(_context2) {
            while (1) switch (_context2.prev = _context2.next) {
              case 0:
                request = window.indexedDB.open(_this.databaseName, _this.dbVersion);

                request.onerror = () => {
                  console.log('数据库打开报错');
                  _this.canuseIndex = false;
                  reject();
                };

                request.onsuccess = /*#__PURE__*/_asyncToGenerator( /*#__PURE__*/_regeneratorRuntime().mark(function _callee() {
                  return _regeneratorRuntime().wrap(function _callee$(_context) {
                    while (1) switch (_context.prev = _context.next) {
                      case 0:
                        _this.db = request.result;
                        console.log('数据库打开成功');
                        _this.canuseIndex = true;
                        resolve(_this.db);

                      case 4:
                      case "end":
                        return _context.stop();
                    }
                  }, _callee);
                }));

                request.onupgradeneeded = event => {
                  _this.canuseIndex = true;
                  _this.db = event.target.result;

                  _this.initTb();

                  const transaction = event.target.transaction;

                  transaction.oncomplete = function () {
                    resolve(this.db);
                  };
                };

              case 4:
              case "end":
                return _context2.stop();
            }
          }, _callee2);
        }));

        return function (_x, _x2) {
          return _ref.apply(this, arguments);
        };
      }());
    }

    return Promise.resolve(null);
  } // 创建表


  createStore(tbName, tconfig) {
    var _this$db;

    if (!this.canuseIndex || !this.haveIndex) return;
    let objectStore;

    if (!((_this$db = this.db) !== null && _this$db !== void 0 && _this$db.objectStoreNames.contains(tbName))) {
      var _this$db2;

      const _ref3 = tconfig || {},
            keyPath = _ref3.keyPath,
            _ref3$indexs = _ref3.indexs,
            indexs = _ref3$indexs === void 0 ? [] : _ref3$indexs,
            config = _objectWithoutProperties(_ref3, _excluded);

      let key = keyPath;
      let indexList = indexs;

      if (!key) {
        key = 'id';
        config.autoIncrement = true;

        if (!indexList.length) {
          indexList = [{
            name: 'id',
            prop: 'id',
            config: {
              unique: true
            }
          }];
        }
      }

      objectStore = (_this$db2 = this.db) === null || _this$db2 === void 0 ? void 0 : _this$db2.createObjectStore(tbName, _objectSpread(_objectSpread({}, config), {}, {
        keyPath: key
      }));
      indexList.forEach(index => this.createIndex(objectStore, index));
    }

    return objectStore;
  } // 创建index


  createIndex(tb, index) {
    if (!tb || !this.db) return false;
    let sotre;

    if (typeof tb === 'string') {
      sotre = this.db.transaction(tb).objectStore(tb);
    } else {
      sotre = tb;
    }

    const name = index.name,
          prop = index.prop,
          config = index.config;
    sotre.createIndex(name, prop, config);
    return true;
  } // 删除表中所有数据


  dropStore(tbName) {
    return new Promise((resolve, reject) => {
      if (!this.canuseIndex || !this.db) return reject();
      const objectStore = this.db.transaction(tbName).objectStore(tbName);
      const request = objectStore.openCursor();

      request.onsuccess = function (event) {
        const cursor = event.target.result;

        if (cursor) {
          cursor.delete();
        } else {
          resolve(true);
        }
      };

      request.onerror = function () {
        reject();
      };
    });
  } // 插入数据


  create(tbName, data) {
    var _this2 = this;

    return new Promise( /*#__PURE__*/function () {
      var _ref4 = _asyncToGenerator( /*#__PURE__*/_regeneratorRuntime().mark(function _callee3(resolve, reject) {
        var keyPath, isExist, request;
        return _regeneratorRuntime().wrap(function _callee3$(_context3) {
          while (1) switch (_context3.prev = _context3.next) {
            case 0:
              if (!(!_this2.canuseIndex || !_this2.db)) {
                _context3.next = 2;
                break;
              }

              return _context3.abrupt("return", reject());

            case 2:
              keyPath = _this2.db.transaction([tbName], 'readwrite').objectStore(tbName).keyPath;
              _context3.next = 5;
              return _this2.findByPk(tbName, data[keyPath]);

            case 5:
              isExist = _context3.sent;
              if (isExist) resolve(true);
              request = _this2.db.transaction([tbName], 'readwrite').objectStore(tbName).add(data);

              request.onsuccess = function () {
                resolve(data);
              };

              request.onerror = function () {
                reject();
              };

            case 10:
            case "end":
              return _context3.stop();
          }
        }, _callee3);
      }));

      return function (_x3, _x4) {
        return _ref4.apply(this, arguments);
      };
    }());
  }

  bulkCreate(tbName, data) {
    var _this3 = this;

    return new Promise( /*#__PURE__*/function () {
      var _ref5 = _asyncToGenerator( /*#__PURE__*/_regeneratorRuntime().mark(function _callee5(resolve, reject) {
        var retList;
        return _regeneratorRuntime().wrap(function _callee5$(_context5) {
          while (1) switch (_context5.prev = _context5.next) {
            case 0:
              if (!(!_this3.canuseIndex || !_this3.db)) {
                _context5.next = 2;
                break;
              }

              return _context5.abrupt("return", reject());

            case 2:
              if (!(!Array.isArray(data) || data.length === 0)) {
                _context5.next = 4;
                break;
              }

              return _context5.abrupt("return", resolve(true));

            case 4:
              _context5.next = 6;
              return Promise.all(data.map( /*#__PURE__*/function () {
                var _ref6 = _asyncToGenerator( /*#__PURE__*/_regeneratorRuntime().mark(function _callee4(d) {
                  return _regeneratorRuntime().wrap(function _callee4$(_context4) {
                    while (1) switch (_context4.prev = _context4.next) {
                      case 0:
                        _context4.next = 2;
                        return _this3.create(tbName, d);

                      case 2:
                        return _context4.abrupt("return", _context4.sent);

                      case 3:
                      case "end":
                        return _context4.stop();
                    }
                  }, _callee4);
                }));

                return function (_x7) {
                  return _ref6.apply(this, arguments);
                };
              }()));

            case 6:
              retList = _context5.sent;
              if (retList.every(t => !!t)) resolve(true);else reject();

            case 8:
            case "end":
              return _context5.stop();
          }
        }, _callee5);
      }));

      return function (_x5, _x6) {
        return _ref5.apply(this, arguments);
      };
    }());
  } // 根据主键查找


  findByPk(tbName, keyPath) {
    return new Promise((resolve, reject) => {
      if (!this.canuseIndex || !this.db) return reject();
      const transaction = this.db.transaction([tbName]);
      const objectStore = transaction.objectStore(tbName);
      const request = objectStore.get(keyPath);

      request.onerror = function () {
        reject();
      };

      request.onsuccess = function () {
        if (request.result) {
          resolve(request.result);
        } else {
          resolve(null);
        }
      };
    });
  } // 根据索引查找


  findByIndex(tbName, indexName, keyPath) {
    return new Promise((resolve, reject) => {
      if (!this.canuseIndex || !this.db) return reject();
      const request = this.db.transaction([tbName]).objectStore(tbName).index(indexName).get(keyPath);

      request.onerror = function () {
        reject();
      };

      request.onsuccess = function (e) {
        const result = e.target.result;

        if (result) {
          resolve(result);
        } else {
          resolve(null);
        }
      };
    });
  } //   遍历指定表


  query(tbName, filter) {
    return new Promise((resolve, reject) => {
      const list = [];
      if (!this.canuseIndex || !this.db) return reject();
      const objectStore = this.db.transaction(tbName).objectStore(tbName);
      const request = objectStore.openCursor();

      request.onsuccess = function (event) {
        const cursor = event.target.result;

        if (cursor) {
          if (!filter || filter(cursor.value)) list.push(cursor.value);
          cursor.continue();
        } else {
          resolve(list);
        }
      };

      request.onerror = function () {
        reject();
      };
    });
  } //   更新指定数据


  update(tbName, data) {
    return new Promise((resolve, reject) => {
      if (!this.canuseIndex || !this.db) return reject();
      const request = this.db.transaction([tbName], 'readwrite').objectStore(tbName).put(data);

      request.onsuccess = function () {
        resolve(data);
      };

      request.onerror = function () {
        reject();
      };
    });
  } // 删除数据


  destroy(tbName, keyPath) {
    return new Promise((resolve, reject) => {
      if (!this.canuseIndex || !this.db) return reject();
      const request = this.db.transaction([tbName], 'readwrite').objectStore(tbName).delete(keyPath);

      request.onsuccess = function () {
        resolve(true);
      };

      request.onerror = function (e) {
        reject(e);
      };
    });
  }

  initTb() {
    this.tbConfigs.forEach(_ref7 => {
      let name = _ref7.name,
          config = _objectWithoutProperties(_ref7, _excluded2);

      this.createStore(name, config);
    });
  }

}

export default IndexdbHelper;