function _slicedToArray(arr, i) { return _arrayWithHoles(arr) || _iterableToArrayLimit(arr, i) || _unsupportedIterableToArray(arr, i) || _nonIterableRest(); }

function _nonIterableRest() { throw new TypeError("Invalid attempt to destructure non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); }

function _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(o); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }

function _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) arr2[i] = arr[i]; return arr2; }

function _iterableToArrayLimit(arr, i) { var _i = arr == null ? null : typeof Symbol !== "undefined" && arr[Symbol.iterator] || arr["@@iterator"]; if (_i == null) return; var _arr = []; var _n = true; var _d = false; var _s, _e; try { for (_i = _i.call(arr); !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"] != null) _i["return"](); } finally { if (_d) throw _e; } } return _arr; }

function _arrayWithHoles(arr) { if (Array.isArray(arr)) return arr; }

import React, { useEffect, useState } from 'react';
import { Button, Tooltip } from 'antd';
import Icon from '../Icon';

const defaultActions = (state = true) => ({
  reUnDo: state,
  zoom: state,
  importData: state,
  exportData: state,
  exportImg: state,
  submit: state,
  copy: state,
  paste: state,
  del: state,
  clear: state,
  plusActions: state
});

const ActionButtons = function ActionButtons({
  onActions,
  isFullScreen = false,
  canRedo = false,
  canUndo = false,
  showActions = true,
  actions,
  appendActions
}) {
  const _useState = useState(defaultActions()),
        _useState2 = _slicedToArray(_useState, 2),
        showState = _useState2[0],
        setShowState = _useState2[1];

  useEffect(() => {
    if (Array.isArray(actions)) {
      const state = defaultActions(false);
      state.plusActions = true;
      actions.forEach(key => {
        switch (key) {
          case 'RE_UN_DO':
            state.reUnDo = true;
            break;

          case 'ZOOM':
            state.zoom = true;
            break;

          case 'IMPORT_DATA':
            state.importData = true;
            state.plusActions = true;
            break;

          case 'EXPORT_DATA':
            state.exportData = true;
            state.plusActions = true;
            break;

          case 'EXPORT_IMG':
            state.exportImg = true;
            state.plusActions = true;
            break;

          case 'COPY':
            state.copy = true;
            state.plusActions = true;
            break;

          case 'PASTE':
            state.paste = true;
            state.plusActions = true;
            break;

          case 'SUBMIT':
            state.submit = true;
            state.plusActions = true;
            break;

          case 'DELETE':
            state.del = true;
            state.plusActions = true;
            break;

          case 'CLEAR':
            state.clear = true;
            state.plusActions = true;
            break;

          default:
            break;
        }
      });
      setShowState(state);
    } else {
      setShowState(defaultActions());
    }
  }, [actions]);
  const reUnDo = showState.reUnDo,
        zoom = showState.zoom,
        importData = showState.importData,
        exportData = showState.exportData,
        exportImg = showState.exportImg,
        submit = showState.submit,
        copy = showState.copy,
        paste = showState.paste,
        clear = showState.clear,
        del = showState.del,
        plusActions = showState.plusActions;
  return /*#__PURE__*/React.createElement("div", {
    className: "ra-arch-actionWrapper"
  }, showActions && /*#__PURE__*/React.createElement("div", {
    className: "ra-arch-actionButtons"
  }, reUnDo && /*#__PURE__*/React.createElement("div", {
    className: "ra-arch-btns-group"
  }, /*#__PURE__*/React.createElement(Tooltip, {
    title: "\u64A4\u9500"
  }, /*#__PURE__*/React.createElement(Button, {
    disabled: !canUndo,
    onClick: () => onActions('undo')
  }, /*#__PURE__*/React.createElement(Icon, {
    type: "icon-undo"
  }))), /*#__PURE__*/React.createElement(Tooltip, {
    title: "\u91CD\u505A"
  }, /*#__PURE__*/React.createElement(Button, {
    disabled: !canRedo,
    onClick: () => onActions('redo')
  }, /*#__PURE__*/React.createElement(Icon, {
    type: "icon-redo"
  })))), zoom && /*#__PURE__*/React.createElement("div", {
    className: "ra-arch-btns-group"
  }, /*#__PURE__*/React.createElement(Tooltip, {
    title: "\u7F29\u5C0F"
  }, /*#__PURE__*/React.createElement(Button, {
    onClick: () => onActions('smaller')
  }, /*#__PURE__*/React.createElement(Icon, {
    type: "icon-smaller"
  }))), /*#__PURE__*/React.createElement(Tooltip, {
    title: "\u653E\u5927"
  }, /*#__PURE__*/React.createElement(Button, {
    onClick: () => onActions('bigger')
  }, /*#__PURE__*/React.createElement(Icon, {
    type: "icon-bigger"
  }))), isFullScreen ? /*#__PURE__*/React.createElement(Tooltip, {
    title: "\u8FD8\u539F"
  }, /*#__PURE__*/React.createElement(Button, {
    onClick: () => onActions('normal')
  }, /*#__PURE__*/React.createElement(Icon, {
    type: "icon-small"
  }))) : /*#__PURE__*/React.createElement(Tooltip, {
    title: "\u5168\u5C4F"
  }, /*#__PURE__*/React.createElement(Button, {
    onClick: () => onActions('full')
  }, /*#__PURE__*/React.createElement(Icon, {
    type: "icon-full"
  })))), plusActions && /*#__PURE__*/React.createElement("div", {
    className: "ra-arch-btns-group"
  }, importData && /*#__PURE__*/React.createElement(Tooltip, {
    title: "\u672C\u5730\u5BFC\u5165\u6570\u636E"
  }, /*#__PURE__*/React.createElement(Button, {
    onClick: () => onActions('importData')
  }, /*#__PURE__*/React.createElement(Icon, {
    type: "icon-import-data"
  }))), exportData && /*#__PURE__*/React.createElement(Tooltip, {
    title: "\u672C\u5730\u5BFC\u51FA\u6570\u636E"
  }, /*#__PURE__*/React.createElement(Button, {
    onClick: () => onActions('exportData')
  }, /*#__PURE__*/React.createElement(Icon, {
    type: "icon-export-data"
  }))), exportImg && /*#__PURE__*/React.createElement(Tooltip, {
    title: "\u672C\u5730\u5BFC\u51FA\u56FE\u7247"
  }, /*#__PURE__*/React.createElement(Button, {
    onClick: () => onActions('exportImg')
  }, /*#__PURE__*/React.createElement(Icon, {
    type: "icon-export-img"
  }))), submit && /*#__PURE__*/React.createElement(Tooltip, {
    title: "\u4E0A\u4F20\u6570\u636E"
  }, /*#__PURE__*/React.createElement(Button, {
    onClick: () => onActions('save')
  }, /*#__PURE__*/React.createElement(Icon, {
    type: "icon-save-clound"
  }))), copy && /*#__PURE__*/React.createElement(Tooltip, {
    title: "\u590D\u5236\u8282\u70B9"
  }, /*#__PURE__*/React.createElement(Button, {
    onClick: () => onActions('copy')
  }, /*#__PURE__*/React.createElement(Icon, {
    type: "icon-copy"
  }))), paste && /*#__PURE__*/React.createElement(Tooltip, {
    title: "\u7C98\u8D34\u8282\u70B9"
  }, /*#__PURE__*/React.createElement(Button, {
    onClick: () => onActions('paste')
  }, /*#__PURE__*/React.createElement(Icon, {
    type: "icon-paste"
  }))), del && /*#__PURE__*/React.createElement(Tooltip, {
    title: "\u5220\u9664\u8282\u70B9"
  }, /*#__PURE__*/React.createElement(Button, {
    onClick: () => onActions('delete')
  }, /*#__PURE__*/React.createElement(Icon, {
    type: "icon-delete"
  }))), clear && /*#__PURE__*/React.createElement(Tooltip, {
    title: "\u6E05\u7406\u7F13\u5B58"
  }, /*#__PURE__*/React.createElement(Button, {
    onClick: () => onActions('clearStorage')
  }, /*#__PURE__*/React.createElement(Icon, {
    type: "icon-clear-storage"
  }))))), /*#__PURE__*/React.createElement("div", {
    className: "ra-arch-systemDesc"
  }, appendActions));
};

export default ActionButtons;