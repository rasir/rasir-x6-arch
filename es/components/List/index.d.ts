import React from 'react';
import { Node } from '@antv/x6';
import { ModuleConfig } from '../../types';
interface ListProps {
    assets: {
        [key: string]: string;
    };
    configs: ModuleConfig;
    onNodeCreate: (node: Node) => void;
    graph: any;
    defaultNodeShape: string;
    moduleNodeShape: string;
}
declare const _default: React.NamedExoticComponent<ListProps>;
export default _default;
