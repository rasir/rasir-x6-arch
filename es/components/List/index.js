import React, { useRef } from 'react';
import { useDebounceEffect } from 'ahooks';
import { Addon } from '@antv/x6';
import { getConfig, appendOptions } from './tools';

const List = function List({
  onNodeCreate,
  configs,
  graph,
  assets,
  defaultNodeShape,
  moduleNodeShape
}) {
  const listRef = useRef(null);

  const initPage = (graph, configs) => {
    if (!graph || !configs || !listRef.current) return;
    const stencilConfig = getConfig(graph, configs, assets, onNodeCreate, defaultNodeShape); // #region 初始化 stencil

    const stencil = new Addon.Stencil(stencilConfig);
    appendOptions(graph, stencil, configs.options, assets, moduleNodeShape);
    listRef.current.appendChild(stencil.container);
  };

  useDebounceEffect(() => {
    initPage(graph, configs);
  }, [graph, configs], {
    wait: 100
  });
  return /*#__PURE__*/React.createElement("div", {
    className: `ra-arch-list ${configs.title ? '' : 'without-title'}`,
    ref: listRef
  });
};

export default /*#__PURE__*/React.memo(List);