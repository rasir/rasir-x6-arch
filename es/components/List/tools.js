function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); enumerableOnly && (symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; })), keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = null != arguments[i] ? arguments[i] : {}; i % 2 ? ownKeys(Object(source), !0).forEach(function (key) { _defineProperty(target, key, source[key]); }) : Object.getOwnPropertyDescriptors ? Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)) : ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

import { getNodeAttr } from '../Lane/tools';
export function getConfig(target, configs, assets, onNodeCreate, defaultNodeShape) {
  const title = configs.title,
        search = configs.search,
        options = configs.options;
  return {
    title,
    target,
    animation: false,
    validateNode: node => {
      var _cells$find;

      const _node$position = node.position(),
            x = _node$position.x,
            y = _node$position.y;

      const _node$size = node.size(),
            w = _node$size.width,
            h = _node$size.height;

      const px = x + w / 2,
            py = y + h / 2;
      const cells = target.getNodesFromPoint({
        x: px,
        y: py
      });
      const preId = (_cells$find = cells.find(cell => cell.data.type === 'lane')) === null || _cells$find === void 0 ? void 0 : _cells$find.id;

      if (preId && onNodeCreate) {
        node.data.preId = preId;
        onNodeCreate(node);
      }

      return false;
    },
    getDragNode: node => {
      return target.createNode({
        shape: defaultNodeShape,
        label: node.label,
        data: node.data,
        type: 'node',
        attrs: _objectSpread({
          body: {
            fill: '#ccc'
          }
        }, getNodeAttr(defaultNodeShape, node.data, assets))
      });
    },
    search: (cell, keyword) => {
      if (keyword) {
        return cell.shape === 'moduleNode' && `${cell.attr('text/text')}`.toLowerCase().includes(`${keyword}`.toLowerCase());
      }

      return true;
    },
    placeholder: (search === null || search === void 0 ? void 0 : search.placeholder) || '请输入组件名称',
    notFoundText: '没有匹配的模型',
    stencilGraphWidth: 200,
    collapsable: false,
    groups: options.map(({
      label,
      value,
      children
    }) => ({
      title: label,
      name: value,
      collapsable: false,
      graphPadding: 0,
      graphHeight: (Array.isArray(children) ? children.length * 32 : 0) + 10,
      layoutOptions: {
        columns: 1,
        columnWidth: 190,
        rowHeight: 32,
        dx: 5,
        dy: 5
      }
    }))
  };
}
export function appendOptions(graph, stencil, options, assets, moduleNodeShape) {
  const moduleProps = {};
  options.forEach(group => {
    const groupName = group.value,
          children = group.children;
    if (!Array.isArray(children)) return;
    const items = children.map(item => {
      const label = item.label,
            value = item.value,
            moduleType = item.moduleType,
            moduleProp = item.moduleProp;
      moduleProps[moduleType || value] = moduleProp;
      item.moduleType = moduleType || value;
      let attrs = {};
      attrs = getNodeAttr(moduleNodeShape, item, assets);
      const node = graph.createNode({
        shape: 'moduleNode',
        label,
        data: item,
        attrs
      });
      return node;
    });
    stencil.load(items, groupName);
  });
  window._moduleProps = moduleProps;
}