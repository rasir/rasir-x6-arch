import React from 'react';
import { SelectOption } from '../../../types';
interface RemoteSelectProps {
    placeholder?: string;
    remote?: (value?: string | number | undefined) => Promise<any>;
    value?: string | number | undefined;
    onChange?: (value?: string | number | undefined) => void;
    onSelected?: (item?: SelectOption | undefined) => void;
    [key: string]: any;
}
declare const RemoteSelect: React.FC<RemoteSelectProps>;
export default RemoteSelect;
