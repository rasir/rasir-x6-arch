const _excluded = ["type", "options"],
      _excluded2 = ["type", "inputProps"];

function _extends() { _extends = Object.assign ? Object.assign.bind() : function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

function _objectWithoutProperties(source, excluded) { if (source == null) return {}; var target = _objectWithoutPropertiesLoose(source, excluded); var key, i; if (Object.getOwnPropertySymbols) { var sourceSymbolKeys = Object.getOwnPropertySymbols(source); for (i = 0; i < sourceSymbolKeys.length; i++) { key = sourceSymbolKeys[i]; if (excluded.indexOf(key) >= 0) continue; if (!Object.prototype.propertyIsEnumerable.call(source, key)) continue; target[key] = source[key]; } } return target; }

function _objectWithoutPropertiesLoose(source, excluded) { if (source == null) return {}; var target = {}; var sourceKeys = Object.keys(source); var key, i; for (i = 0; i < sourceKeys.length; i++) { key = sourceKeys[i]; if (excluded.indexOf(key) >= 0) continue; target[key] = source[key]; } return target; }

// 基础搜索组件通过简单配置生成搜索表单
import React from 'react';
import { Input, Form, Select, DatePicker } from 'antd';
import RemoteSelect from './RemoteSelect';
const RangePicker = DatePicker.RangePicker;
export const BaseItem = function BaseItem(_ref) {
  let type = _ref.type,
      options = _ref.options,
      props = _objectWithoutProperties(_ref, _excluded);

  if (!props.style) props.style = {};

  if (!props.style.width) {
    props.style.width = '100%';
  }

  return /*#__PURE__*/React.createElement(React.Fragment, null, type === 'INPUT' && /*#__PURE__*/React.createElement(Input, props), type === 'TEXTAREA' && /*#__PURE__*/React.createElement(Input.TextArea, props), type === 'SEARCH' && /*#__PURE__*/React.createElement(Input.Search, props), type === 'REMOTESELECT' && /*#__PURE__*/React.createElement(RemoteSelect, props), type === 'SELECT' && /*#__PURE__*/React.createElement(Select, props, options === null || options === void 0 ? void 0 : options.map(e => /*#__PURE__*/React.createElement(Select.Option, {
    key: e.value,
    value: e.value
  }, e.label))), type === 'DATETIMERANGER' && /*#__PURE__*/React.createElement(RangePicker, _extends({
    showTime: true
  }, props)), type === 'DATERANGER' && /*#__PURE__*/React.createElement(RangePicker, props));
};
const formLayout = {
  wrapperCol: {
    span: 16
  },
  labelCol: {
    span: 8
  }
};

const SearchBase = function SearchBase({
  items = [],
  form
}) {
  return /*#__PURE__*/React.createElement(Form, _extends({}, formLayout, {
    className: "ra-arch-searchBase",
    style: {
      background: '#fff'
    },
    form: form
  }), items.map((_ref2, index) => {
    let type = _ref2.type,
        inputProps = _ref2.inputProps,
        props = _objectWithoutProperties(_ref2, _excluded2);

    return /*#__PURE__*/React.createElement(Form.Item, _extends({
      key: `${index + 1}`
    }, props), /*#__PURE__*/React.createElement(BaseItem, _extends({
      type: type
    }, inputProps)));
  }));
};

export default SearchBase;