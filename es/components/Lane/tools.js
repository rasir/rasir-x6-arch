function _regeneratorRuntime() { "use strict"; /*! regenerator-runtime -- Copyright (c) 2014-present, Facebook, Inc. -- license (MIT): https://github.com/facebook/regenerator/blob/main/LICENSE */ _regeneratorRuntime = function _regeneratorRuntime() { return exports; }; var exports = {}, Op = Object.prototype, hasOwn = Op.hasOwnProperty, $Symbol = "function" == typeof Symbol ? Symbol : {}, iteratorSymbol = $Symbol.iterator || "@@iterator", asyncIteratorSymbol = $Symbol.asyncIterator || "@@asyncIterator", toStringTagSymbol = $Symbol.toStringTag || "@@toStringTag"; function define(obj, key, value) { return Object.defineProperty(obj, key, { value: value, enumerable: !0, configurable: !0, writable: !0 }), obj[key]; } try { define({}, ""); } catch (err) { define = function define(obj, key, value) { return obj[key] = value; }; } function wrap(innerFn, outerFn, self, tryLocsList) { var protoGenerator = outerFn && outerFn.prototype instanceof Generator ? outerFn : Generator, generator = Object.create(protoGenerator.prototype), context = new Context(tryLocsList || []); return generator._invoke = function (innerFn, self, context) { var state = "suspendedStart"; return function (method, arg) { if ("executing" === state) throw new Error("Generator is already running"); if ("completed" === state) { if ("throw" === method) throw arg; return doneResult(); } for (context.method = method, context.arg = arg;;) { var delegate = context.delegate; if (delegate) { var delegateResult = maybeInvokeDelegate(delegate, context); if (delegateResult) { if (delegateResult === ContinueSentinel) continue; return delegateResult; } } if ("next" === context.method) context.sent = context._sent = context.arg;else if ("throw" === context.method) { if ("suspendedStart" === state) throw state = "completed", context.arg; context.dispatchException(context.arg); } else "return" === context.method && context.abrupt("return", context.arg); state = "executing"; var record = tryCatch(innerFn, self, context); if ("normal" === record.type) { if (state = context.done ? "completed" : "suspendedYield", record.arg === ContinueSentinel) continue; return { value: record.arg, done: context.done }; } "throw" === record.type && (state = "completed", context.method = "throw", context.arg = record.arg); } }; }(innerFn, self, context), generator; } function tryCatch(fn, obj, arg) { try { return { type: "normal", arg: fn.call(obj, arg) }; } catch (err) { return { type: "throw", arg: err }; } } exports.wrap = wrap; var ContinueSentinel = {}; function Generator() {} function GeneratorFunction() {} function GeneratorFunctionPrototype() {} var IteratorPrototype = {}; define(IteratorPrototype, iteratorSymbol, function () { return this; }); var getProto = Object.getPrototypeOf, NativeIteratorPrototype = getProto && getProto(getProto(values([]))); NativeIteratorPrototype && NativeIteratorPrototype !== Op && hasOwn.call(NativeIteratorPrototype, iteratorSymbol) && (IteratorPrototype = NativeIteratorPrototype); var Gp = GeneratorFunctionPrototype.prototype = Generator.prototype = Object.create(IteratorPrototype); function defineIteratorMethods(prototype) { ["next", "throw", "return"].forEach(function (method) { define(prototype, method, function (arg) { return this._invoke(method, arg); }); }); } function AsyncIterator(generator, PromiseImpl) { function invoke(method, arg, resolve, reject) { var record = tryCatch(generator[method], generator, arg); if ("throw" !== record.type) { var result = record.arg, value = result.value; return value && "object" == typeof value && hasOwn.call(value, "__await") ? PromiseImpl.resolve(value.__await).then(function (value) { invoke("next", value, resolve, reject); }, function (err) { invoke("throw", err, resolve, reject); }) : PromiseImpl.resolve(value).then(function (unwrapped) { result.value = unwrapped, resolve(result); }, function (error) { return invoke("throw", error, resolve, reject); }); } reject(record.arg); } var previousPromise; this._invoke = function (method, arg) { function callInvokeWithMethodAndArg() { return new PromiseImpl(function (resolve, reject) { invoke(method, arg, resolve, reject); }); } return previousPromise = previousPromise ? previousPromise.then(callInvokeWithMethodAndArg, callInvokeWithMethodAndArg) : callInvokeWithMethodAndArg(); }; } function maybeInvokeDelegate(delegate, context) { var method = delegate.iterator[context.method]; if (undefined === method) { if (context.delegate = null, "throw" === context.method) { if (delegate.iterator.return && (context.method = "return", context.arg = undefined, maybeInvokeDelegate(delegate, context), "throw" === context.method)) return ContinueSentinel; context.method = "throw", context.arg = new TypeError("The iterator does not provide a 'throw' method"); } return ContinueSentinel; } var record = tryCatch(method, delegate.iterator, context.arg); if ("throw" === record.type) return context.method = "throw", context.arg = record.arg, context.delegate = null, ContinueSentinel; var info = record.arg; return info ? info.done ? (context[delegate.resultName] = info.value, context.next = delegate.nextLoc, "return" !== context.method && (context.method = "next", context.arg = undefined), context.delegate = null, ContinueSentinel) : info : (context.method = "throw", context.arg = new TypeError("iterator result is not an object"), context.delegate = null, ContinueSentinel); } function pushTryEntry(locs) { var entry = { tryLoc: locs[0] }; 1 in locs && (entry.catchLoc = locs[1]), 2 in locs && (entry.finallyLoc = locs[2], entry.afterLoc = locs[3]), this.tryEntries.push(entry); } function resetTryEntry(entry) { var record = entry.completion || {}; record.type = "normal", delete record.arg, entry.completion = record; } function Context(tryLocsList) { this.tryEntries = [{ tryLoc: "root" }], tryLocsList.forEach(pushTryEntry, this), this.reset(!0); } function values(iterable) { if (iterable) { var iteratorMethod = iterable[iteratorSymbol]; if (iteratorMethod) return iteratorMethod.call(iterable); if ("function" == typeof iterable.next) return iterable; if (!isNaN(iterable.length)) { var i = -1, next = function next() { for (; ++i < iterable.length;) if (hasOwn.call(iterable, i)) return next.value = iterable[i], next.done = !1, next; return next.value = undefined, next.done = !0, next; }; return next.next = next; } } return { next: doneResult }; } function doneResult() { return { value: undefined, done: !0 }; } return GeneratorFunction.prototype = GeneratorFunctionPrototype, define(Gp, "constructor", GeneratorFunctionPrototype), define(GeneratorFunctionPrototype, "constructor", GeneratorFunction), GeneratorFunction.displayName = define(GeneratorFunctionPrototype, toStringTagSymbol, "GeneratorFunction"), exports.isGeneratorFunction = function (genFun) { var ctor = "function" == typeof genFun && genFun.constructor; return !!ctor && (ctor === GeneratorFunction || "GeneratorFunction" === (ctor.displayName || ctor.name)); }, exports.mark = function (genFun) { return Object.setPrototypeOf ? Object.setPrototypeOf(genFun, GeneratorFunctionPrototype) : (genFun.__proto__ = GeneratorFunctionPrototype, define(genFun, toStringTagSymbol, "GeneratorFunction")), genFun.prototype = Object.create(Gp), genFun; }, exports.awrap = function (arg) { return { __await: arg }; }, defineIteratorMethods(AsyncIterator.prototype), define(AsyncIterator.prototype, asyncIteratorSymbol, function () { return this; }), exports.AsyncIterator = AsyncIterator, exports.async = function (innerFn, outerFn, self, tryLocsList, PromiseImpl) { void 0 === PromiseImpl && (PromiseImpl = Promise); var iter = new AsyncIterator(wrap(innerFn, outerFn, self, tryLocsList), PromiseImpl); return exports.isGeneratorFunction(outerFn) ? iter : iter.next().then(function (result) { return result.done ? result.value : iter.next(); }); }, defineIteratorMethods(Gp), define(Gp, toStringTagSymbol, "Generator"), define(Gp, iteratorSymbol, function () { return this; }), define(Gp, "toString", function () { return "[object Generator]"; }), exports.keys = function (object) { var keys = []; for (var key in object) keys.push(key); return keys.reverse(), function next() { for (; keys.length;) { var key = keys.pop(); if (key in object) return next.value = key, next.done = !1, next; } return next.done = !0, next; }; }, exports.values = values, Context.prototype = { constructor: Context, reset: function reset(skipTempReset) { if (this.prev = 0, this.next = 0, this.sent = this._sent = undefined, this.done = !1, this.delegate = null, this.method = "next", this.arg = undefined, this.tryEntries.forEach(resetTryEntry), !skipTempReset) for (var name in this) "t" === name.charAt(0) && hasOwn.call(this, name) && !isNaN(+name.slice(1)) && (this[name] = undefined); }, stop: function stop() { this.done = !0; var rootRecord = this.tryEntries[0].completion; if ("throw" === rootRecord.type) throw rootRecord.arg; return this.rval; }, dispatchException: function dispatchException(exception) { if (this.done) throw exception; var context = this; function handle(loc, caught) { return record.type = "throw", record.arg = exception, context.next = loc, caught && (context.method = "next", context.arg = undefined), !!caught; } for (var i = this.tryEntries.length - 1; i >= 0; --i) { var entry = this.tryEntries[i], record = entry.completion; if ("root" === entry.tryLoc) return handle("end"); if (entry.tryLoc <= this.prev) { var hasCatch = hasOwn.call(entry, "catchLoc"), hasFinally = hasOwn.call(entry, "finallyLoc"); if (hasCatch && hasFinally) { if (this.prev < entry.catchLoc) return handle(entry.catchLoc, !0); if (this.prev < entry.finallyLoc) return handle(entry.finallyLoc); } else if (hasCatch) { if (this.prev < entry.catchLoc) return handle(entry.catchLoc, !0); } else { if (!hasFinally) throw new Error("try statement without catch or finally"); if (this.prev < entry.finallyLoc) return handle(entry.finallyLoc); } } } }, abrupt: function abrupt(type, arg) { for (var i = this.tryEntries.length - 1; i >= 0; --i) { var entry = this.tryEntries[i]; if (entry.tryLoc <= this.prev && hasOwn.call(entry, "finallyLoc") && this.prev < entry.finallyLoc) { var finallyEntry = entry; break; } } finallyEntry && ("break" === type || "continue" === type) && finallyEntry.tryLoc <= arg && arg <= finallyEntry.finallyLoc && (finallyEntry = null); var record = finallyEntry ? finallyEntry.completion : {}; return record.type = type, record.arg = arg, finallyEntry ? (this.method = "next", this.next = finallyEntry.finallyLoc, ContinueSentinel) : this.complete(record); }, complete: function complete(record, afterLoc) { if ("throw" === record.type) throw record.arg; return "break" === record.type || "continue" === record.type ? this.next = record.arg : "return" === record.type ? (this.rval = this.arg = record.arg, this.method = "return", this.next = "end") : "normal" === record.type && afterLoc && (this.next = afterLoc), ContinueSentinel; }, finish: function finish(finallyLoc) { for (var i = this.tryEntries.length - 1; i >= 0; --i) { var entry = this.tryEntries[i]; if (entry.finallyLoc === finallyLoc) return this.complete(entry.completion, entry.afterLoc), resetTryEntry(entry), ContinueSentinel; } }, catch: function _catch(tryLoc) { for (var i = this.tryEntries.length - 1; i >= 0; --i) { var entry = this.tryEntries[i]; if (entry.tryLoc === tryLoc) { var record = entry.completion; if ("throw" === record.type) { var thrown = record.arg; resetTryEntry(entry); } return thrown; } } throw new Error("illegal catch attempt"); }, delegateYield: function delegateYield(iterable, resultName, nextLoc) { return this.delegate = { iterator: values(iterable), resultName: resultName, nextLoc: nextLoc }, "next" === this.method && (this.arg = undefined), ContinueSentinel; } }, exports; }

function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

import { Graph, Shape } from '@antv/x6';
import { message } from 'antd';
import { autoSaveJson, defaultEdgeShapeName, defaultModuleNodeName } from '../../tools';
const minimumLaneHeight = 50;
const nodeHanlderMap = new Map(); // 获取节点样式

export function getNodeAttr(shape, data, assets) {
  if (!shape) return {};
  const handler = nodeHanlderMap.get(shape);

  if (handler) {
    return handler(data, assets);
  }

  return {};
} // 注册节点和线条的样式

export function registerAll(list) {
  if (!Array.isArray(list) || list.length === 0) return;
  list.forEach(({
    name,
    options,
    type,
    handler
  }) => {
    if (type === 'NODE' && handler) {
      nodeHanlderMap.set(name, handler);
      Graph.registerNode(name, options, true);
    } else if (type === 'EDGE') {
      Graph.registerEdge(name, options, true);
    }
  });
} // 默认模型组件样式

function getDefaultModuleNodeAttr(data, assets) {
  const icon = data.icon;
  const image = assets[icon] || icon;
  return {
    icon: {
      xlinkHref: image
    }
  };
} // 注册一个默认的样板组件


export function registerModuleNode() {
  nodeHanlderMap.set(defaultModuleNodeName, getDefaultModuleNodeAttr);
  Graph.registerNode(defaultModuleNodeName, {
    inherit: 'rect',
    width: 190,
    height: 32,
    markup: [{
      tagName: 'rect',
      selector: 'body'
    }, {
      tagName: 'text',
      selector: 'text'
    }, {
      tagName: 'image',
      selector: 'icon'
    }],
    attrs: {
      body: {
        strokeWidth: 0,
        stroke: '#5F95FF',
        fill: '#fff'
      },
      icon: {
        ref: 'body',
        refX: 10,
        refY: 8,
        width: 16,
        height: 16
      },
      text: {
        refX: 36,
        refY: 17,
        fontSize: 12,
        fill: '#333',
        textAnchor: 'start'
      }
    }
  }, true);
} // 泳道样式

export function registerLane(config = {}) {
  const _config$width = config.width,
        width = _config$width === void 0 ? 500 : _config$width,
        _config$height = config.height,
        height = _config$height === void 0 ? 200 : _config$height;
  Graph.registerNode('lane', {
    width,
    height,
    inherit: 'rect',
    markup: [{
      tagName: 'rect',
      selector: 'body',
      className: 'laneBody'
    }, {
      tagName: 'rect',
      selector: 'nameRect',
      className: 'laneRect'
    }, {
      tagName: 'text',
      selector: 'nameText',
      className: 'laneName'
    }, {
      tagName: 'rect',
      selector: 'bottomBorder',
      className: 'resizeBorder'
    }],
    attrs: {
      body: {
        fill: 'transparent',
        stroke: '#f00',
        strokeWidth: 0,
        cursor: 'default'
      },
      nameRect: {
        width: 30,
        refHeight: '100%',
        fill: '#EAFEEC',
        stroke: '#fff',
        strokeWidth: 0,
        x: 0,
        cursor: 'default'
      },
      nameText: {
        ref: 'nameRect',
        textAnchor: 'middle',
        fontWeight: 'bold',
        fill: '#6A8E67',
        fontSize: 12,
        cursor: 'default'
      },
      bottomBorder: {
        refWidth: '100%',
        height: 2,
        stroke: '#9BD787',
        refY: '100%',
        refX: 0,
        y: -3,
        cursor: 'row-resize'
      }
    }
  }, true);
} // 默认线条

export function registerEdge() {
  Graph.registerEdge(defaultEdgeShapeName, {
    inherit: 'edge',
    attrs: {
      line: {
        stroke: '#A2B1C3',
        strokeWidth: 2
      }
    },
    label: {
      attrs: {
        label: {
          fill: '#A2B1C3',
          fontSize: 12
        }
      }
    }
  }, true);
}

function scroll(startPoint, currentPoint, xAxisRef, yAxisRef) {
  const _ref = startPoint || {
    x: 0,
    y: 0
  },
        sx = _ref.x,
        sy = _ref.y;

  const cx = currentPoint.x,
        cy = currentPoint.y;
  const ox = sx - cx,
        oy = sy - cy;
  xAxisRef.current.scrollLeft = xAxisRef.current.scrollLeft + ox;
  yAxisRef.current.scrollTop = yAxisRef.current.scrollTop + oy;
  startPoint.x = cx;
  startPoint.y = cy;
} // 拖拽移动画面


export function registerDragGraph(graph, xAxisRef, yAxisRef) {
  let startPoint;
  graph.on('node:mousedown', ({
    e,
    cell
  }) => {
    const target = e.target,
          x = e.clientX,
          y = e.offsetY;
    const type = cell.data.type;

    if (target && type === 'lane') {
      startPoint = {
        x,
        y
      };
    }
  });
  graph.on('node:mousemove', ({
    e,
    cell
  }) => {
    const target = e.target,
          x = e.clientX,
          y = e.clientY;
    const type = cell.data.type;

    if (target && type === 'lane') {
      scroll(startPoint, {
        x,
        y
      }, xAxisRef, yAxisRef);
    }
  });
  graph.on('node:mouseup', () => {
    startPoint = undefined;
  });
  graph.on('blank:mousedown', ({
    e
  }) => {
    const x = e.clientX,
          y = e.clientY;
    startPoint = {
      x,
      y
    };
  });
  graph.on('blank:mousemove', ({
    e
  }) => {
    const x = e.clientX,
          y = e.clientY;
    scroll(startPoint, {
      x,
      y
    }, xAxisRef, yAxisRef);
  });
  graph.on('blank:mouseup', () => {
    startPoint = undefined;
  });
  yAxisRef.current.addEventListener('mousewheel', mouseScroll); // 鼠标滚轮左右滚动

  function mouseScroll(e) {
    const x = e.wheelDeltaX,
          y = e.wheelDeltaY;
    scroll({
      x: 0,
      y: 0
    }, {
      x,
      y
    }, xAxisRef, yAxisRef);
  }

  return function () {
    if (yAxisRef.current) {
      yAxisRef.current.removeEventListener('mousewheel', mouseScroll);
    }
  };
} // 控制连接桩显示/隐藏

const showPorts = (ports, show) => {
  if (!ports) return;

  for (let i = 0, len = ports.length; i < len; i = i + 1) {
    ports[i].style.visibility = show ? 'visible' : 'hidden';
  }
}; // 注册泳道的拖拽改变尺寸


export function registerResizeLane(graph, onLaneResize) {
  let dragNode = undefined;
  let offsetY = undefined;
  let oHeight = undefined;
  let minHeight = minimumLaneHeight;
  graph.on('node:mousedown', ({
    e,
    x,
    y,
    cell,
    node
  }) => {
    const target = e.target;

    if (target && target.className.baseVal === 'resizeBorder') {
      dragNode = cell;
      offsetY = y;
      oHeight = node.size().height;
      const children = node.getChildren();

      if (Array.isArray(children) && children.length) {
        minHeight = Math.max(...children.map(child => child.size().height));
      }
    }
  });
  graph.on('node:mousemove', ({
    y,
    node
  }) => {
    if (!dragNode) return;

    const _node$size = node.size(),
          width = _node$size.width;

    const height = (oHeight || 0) + y - (offsetY || 0);
    onLaneResize && onLaneResize(dragNode, width, height, minHeight);
  });
  graph.on('node:mouseup', () => {
    dragNode = undefined;
    offsetY = undefined;
    oHeight = undefined;
    minHeight = minimumLaneHeight;
  });
} // 节点双击事件

export function registerDbClickNode(graph, setCurrentCell, handlerActions) {
  graph.on('node:dblclick', ({
    node
  }) => {
    if (node.data.type !== 'node') return;
    setCurrentCell(node);
    handlerActions && handlerActions('edite');
  });
} // 节点连线点显示的事件

export function registerResizeNode(graph) {
  graph.on('node:mouseenter', ({
    node,
    view
  }) => {
    if (node.data.type !== 'node') return;
    const container = view.container;
    const ports = container.querySelectorAll('.x6-port-body');
    showPorts(ports, true);
  });
  graph.on('node:mouseleave', ({
    node,
    view
  }) => {
    if (node.data.type !== 'node') return;
    const container = view.container;
    const ports = container.querySelectorAll('.x6-port-body');
    showPorts(ports, false);
  });
} // 右键显示按钮

export function registerNodeMenuContext(graph, menuRef, callback) {
  graph.on('node:contextmenu', ({
    e,
    x,
    y,
    cell
  }) => {
    if (cell.data.type === 'lane') return;
    const menuContainer = menuRef.current;
    const clientWidth = menuContainer.clientWidth,
          clientHeight = menuContainer.clientHeight;
    menuContainer.style.left = `${x - clientWidth / 2}px`;
    menuContainer.style.top = `${y - clientHeight / 2}px`;
    callback && callback(cell);
  });
} // 监听元素变化（节点和线）

export function registerAutoSave(graph, callback) {
  graph.on('cell:added', /*#__PURE__*/_asyncToGenerator( /*#__PURE__*/_regeneratorRuntime().mark(function _callee() {
    return _regeneratorRuntime().wrap(function _callee$(_context) {
      while (1) switch (_context.prev = _context.next) {
        case 0:
          _context.next = 2;
          return autoSaveJson(null, graph, callback);

        case 2:
        case "end":
          return _context.stop();
      }
    }, _callee);
  })));
  graph.on('cell:removed', /*#__PURE__*/_asyncToGenerator( /*#__PURE__*/_regeneratorRuntime().mark(function _callee2() {
    return _regeneratorRuntime().wrap(function _callee2$(_context2) {
      while (1) switch (_context2.prev = _context2.next) {
        case 0:
          _context2.next = 2;
          return autoSaveJson(null, graph, callback);

        case 2:
        case "end":
          return _context2.stop();
      }
    }, _callee2);
  })));
  graph.on('cell:changed', /*#__PURE__*/_asyncToGenerator( /*#__PURE__*/_regeneratorRuntime().mark(function _callee3() {
    return _regeneratorRuntime().wrap(function _callee3$(_context3) {
      while (1) switch (_context3.prev = _context3.next) {
        case 0:
          _context3.next = 2;
          return autoSaveJson(null, graph, callback);

        case 2:
        case "end":
          return _context3.stop();
      }
    }, _callee3);
  })));
} // 删除连线

export function registerEdgeRemove(graph, callback) {
  graph.on('edge:mouseenter', ({
    edge
  }) => {
    edge.addTools([{
      name: 'button-remove',
      args: {
        distance: -40,

        onClick({
          view
        }) {
          const edge = view.cell;
          callback && callback(edge);
        }

      }
    }]);
  });
  graph.on('edge:mouseleave', ({
    edge
  }) => {
    edge.removeTools();
  });
} // 节点数量发生变化时

export function registerNodeNumChange(graph) {
  graph.on('node:added', ({
    node
  }) => {
    if (!node.parent) return;

    const _node$size2 = node.size(),
          nw = _node$size2.width;

    const gw = graph.container.clientWidth;
    const lane = graph.getCellById(node.parent.id);
    const nodes = lane.getChildren();
    const maxWidth = Math.max(gw, ...(nodes || []).map(n => n.position().x + n.size().width + nw + 100));

    if (maxWidth !== gw) {
      graph.getNodes().filter(n => n.shape === 'lane').forEach(laneNode => {
        const _laneNode$size = laneNode.size(),
              lh = _laneNode$size.height;

        laneNode.size(maxWidth, lh);
      });
      graph.container.style.width = maxWidth + 'px';
    }
  });
} // 监听history变化

export function registerHistory(graph, configs) {
  const setCanRedo = configs.setCanRedo,
        setCanUndo = configs.setCanUndo,
        onUndoOrRedo = configs.onUndoOrRedo; // 节点和边的添加和移除可能涉及到后端交互，因此这里交给开发者处理，组件不做处理

  graph.history.on('undo', args => {
    onUndoOrRedo && onUndoOrRedo('undo', graph, args);
  });
  graph.history.on('redo', args => {
    onUndoOrRedo && onUndoOrRedo('redo', graph, args);
  });
  graph.history.on('change', () => {
    setCanRedo(graph.history.canRedo());
    setCanUndo(graph.history.canUndo());
  });
} // 泳道高度调整

export function registerLaneCellChange(cell, onLaneSizeChanged) {
  cell.on('change:size', () => onLaneSizeChanged());
} // 注册快捷键

export function registerKeyboard(graph, onActions) {
  // #region 快捷键与事件
  // copy
  graph.bindKey(['meta+c', 'ctrl+c'], () => {
    onActions && onActions('copy');
    return false;
  }); // paste

  graph.bindKey(['meta+v', 'ctrl+v'], () => {
    onActions && onActions('paste');
    return false;
  }); //undo

  graph.bindKey(['meta+z', 'ctrl+z'], () => {
    onActions && onActions('undo');
    return false;
  }); // redo

  graph.bindKey(['meta+shift+z', 'ctrl+shift+z'], () => {
    onActions && onActions('redo');
    return false;
  }); //delete

  graph.bindKey('backspace', () => {
    onActions && onActions('delete');
  }); // zoom bigger

  graph.bindKey(['ctrl+1', 'meta+1'], () => {
    onActions && onActions('bigger');
  }); // zoom smaller

  graph.bindKey(['ctrl+2', 'meta+2'], () => {
    onActions && onActions('smaller');
  }); // clearStorage

  graph.bindKey(['ctrl+e', 'meta+e'], () => {
    onActions && onActions('clearStorage');
  });
} // 初始化画布

export function getGraph(container, minimapContainer, config) {
  const _ref5 = config || {},
        onAddLink = _ref5.onAddLink,
        defaultEdgeShape = _ref5.defaultEdgeShape;

  const graph = new Graph({
    panning: false,
    keyboard: true,
    history: {
      enabled: true,
      ignoreAdd: false,
      ignoreRemove: false,
      ignoreChange: false,

      beforeAddCommand(event, args) {
        const key = args.key,
              cell = args.cell;
        if (args.key === 'tools') return false; // 数据如果影响到节点样式会被记录到撤销队列，但是撤销的时候只能恢复样式，无法恢复数据
        // 因此禁止节点数据更新被记录到队列中

        if (event === 'cell:change:*' && cell.isNode() && key === 'attrs') {
          return false;
        }

        if (cell.isEdge()) {
          if (!cell.data) return false;
          const _cell$data = cell.data,
                source = _cell$data.source,
                target = _cell$data.target;
          if (!source || !target) return false;
        }

        return true;
      }

    },
    grid: {
      size: 10,
      visible: true,
      type: 'mesh'
    },
    container: container,
    selecting: {
      enabled: true,

      filter(node) {
        return node.shape !== 'lane';
      }

    },
    clipboard: true,
    resizing: {
      enabled: cell => {
        return cell.type === 'node';
      }
    },
    connecting: {
      allowNode: options => {
        const targetCell = options.targetCell;
        return targetCell.type === 'node';
      },
      router: 'manhattan',
      connector: {
        name: 'rounded',
        args: {
          radius: 8
        }
      },
      anchor: 'center',
      allowBlank: false,
      allowEdge: false,
      allowLoop: false,
      allowMulti: false,
      snap: {
        radius: 20
      },

      createEdge() {
        return new Shape.Edge({
          shape: defaultEdgeShape,
          attrs: {
            line: {
              stroke: '#A2B1C3',
              strokeWidth: 2
            }
          }
        });
      },

      validateEdge({
        edge
      }) {
        const sourceCell = edge.getSourceCell();
        const targetCell = edge.getTargetCell();

        if (!sourceCell || !targetCell || !sourceCell.parent || !targetCell.parent) {
          return false;
        }

        const sourceParentSort = sourceCell.parent.data.sort;
        const targetParentSort = targetCell.parent.data.sort; // 只能相邻层级节点进行连线

        if (Math.abs(sourceParentSort - targetParentSort) !== 1) {
          message.info('只能对相邻的层级进行连线');
          return false;
        }

        edge.data = {
          id: `${sourceCell.id}_${targetCell.id}`,
          type: 'link'
        };
        onAddLink && onAddLink(edge);
        return false;
      }

    },
    translating: {
      restrict(cellView) {
        const cell = cellView.cell;
        const parentId = cell.prop('parent');

        if (parentId) {
          const parentNode = graph.getCellById(parentId);

          if (parentNode) {
            return parentNode.getBBox().moveAndExpand({
              x: 32,
              y: 0,
              width: -32,
              height: -4
            });
          }
        }

        return cell.getBBox();
      }

    }
  });
  return graph;
}