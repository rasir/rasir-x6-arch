const _excluded = ["type", "className", "color"];

function _extends() { _extends = Object.assign ? Object.assign.bind() : function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

function _objectWithoutProperties(source, excluded) { if (source == null) return {}; var target = _objectWithoutPropertiesLoose(source, excluded); var key, i; if (Object.getOwnPropertySymbols) { var sourceSymbolKeys = Object.getOwnPropertySymbols(source); for (i = 0; i < sourceSymbolKeys.length; i++) { key = sourceSymbolKeys[i]; if (excluded.indexOf(key) >= 0) continue; if (!Object.prototype.propertyIsEnumerable.call(source, key)) continue; target[key] = source[key]; } } return target; }

function _objectWithoutPropertiesLoose(source, excluded) { if (source == null) return {}; var target = {}; var sourceKeys = Object.keys(source); var key, i; for (i = 0; i < sourceKeys.length; i++) { key = sourceKeys[i]; if (excluded.indexOf(key) >= 0) continue; target[key] = source[key]; } return target; }

// iconfont 图标组件
import React from 'react'; // import './index.less';

const Icon = function Icon(_ref) {
  let type = _ref.type,
      _ref$className = _ref.className,
      className = _ref$className === void 0 ? '' : _ref$className,
      color = _ref.color,
      prpos = _objectWithoutProperties(_ref, _excluded);

  return /*#__PURE__*/React.createElement("span", null, /*#__PURE__*/React.createElement("svg", _extends({
    className: `ra-icon ${className}`,
    "aria-hidden": "true",
    style: {
      color
    }
  }, prpos), /*#__PURE__*/React.createElement("use", {
    xlinkHref: `#${type}`
  })));
};

export default Icon;