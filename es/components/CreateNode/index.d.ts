import React from 'react';
import { FormInstance } from 'antd';
import { Node } from '@antv/x6';
interface CreateNodeProps {
    visible?: boolean;
    onClose: () => void;
    onSubmit?: (nodeData: any) => Promise<boolean>;
    node?: Node;
    onLoad?: (form: FormInstance) => void;
    rootNode?: false | string | HTMLElement | (() => HTMLElement);
}
declare const CreateNode: React.FC<CreateNodeProps>;
export default CreateNode;
