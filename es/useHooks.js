function _slicedToArray(arr, i) { return _arrayWithHoles(arr) || _iterableToArrayLimit(arr, i) || _unsupportedIterableToArray(arr, i) || _nonIterableRest(); }

function _nonIterableRest() { throw new TypeError("Invalid attempt to destructure non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); }

function _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(o); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }

function _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) arr2[i] = arr[i]; return arr2; }

function _iterableToArrayLimit(arr, i) { var _i = arr == null ? null : typeof Symbol !== "undefined" && arr[Symbol.iterator] || arr["@@iterator"]; if (_i == null) return; var _arr = []; var _n = true; var _d = false; var _s, _e; try { for (_i = _i.call(arr); !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"] != null) _i["return"](); } finally { if (_d) throw _e; } } return _arr; }

function _arrayWithHoles(arr) { if (Array.isArray(arr)) return arr; }

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); enumerableOnly && (symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; })), keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = null != arguments[i] ? arguments[i] : {}; i % 2 ? ownKeys(Object(source), !0).forEach(function (key) { _defineProperty(target, key, source[key]); }) : Object.getOwnPropertyDescriptors ? Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)) : ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function _regeneratorRuntime() { "use strict"; /*! regenerator-runtime -- Copyright (c) 2014-present, Facebook, Inc. -- license (MIT): https://github.com/facebook/regenerator/blob/main/LICENSE */ _regeneratorRuntime = function _regeneratorRuntime() { return exports; }; var exports = {}, Op = Object.prototype, hasOwn = Op.hasOwnProperty, $Symbol = "function" == typeof Symbol ? Symbol : {}, iteratorSymbol = $Symbol.iterator || "@@iterator", asyncIteratorSymbol = $Symbol.asyncIterator || "@@asyncIterator", toStringTagSymbol = $Symbol.toStringTag || "@@toStringTag"; function define(obj, key, value) { return Object.defineProperty(obj, key, { value: value, enumerable: !0, configurable: !0, writable: !0 }), obj[key]; } try { define({}, ""); } catch (err) { define = function define(obj, key, value) { return obj[key] = value; }; } function wrap(innerFn, outerFn, self, tryLocsList) { var protoGenerator = outerFn && outerFn.prototype instanceof Generator ? outerFn : Generator, generator = Object.create(protoGenerator.prototype), context = new Context(tryLocsList || []); return generator._invoke = function (innerFn, self, context) { var state = "suspendedStart"; return function (method, arg) { if ("executing" === state) throw new Error("Generator is already running"); if ("completed" === state) { if ("throw" === method) throw arg; return doneResult(); } for (context.method = method, context.arg = arg;;) { var delegate = context.delegate; if (delegate) { var delegateResult = maybeInvokeDelegate(delegate, context); if (delegateResult) { if (delegateResult === ContinueSentinel) continue; return delegateResult; } } if ("next" === context.method) context.sent = context._sent = context.arg;else if ("throw" === context.method) { if ("suspendedStart" === state) throw state = "completed", context.arg; context.dispatchException(context.arg); } else "return" === context.method && context.abrupt("return", context.arg); state = "executing"; var record = tryCatch(innerFn, self, context); if ("normal" === record.type) { if (state = context.done ? "completed" : "suspendedYield", record.arg === ContinueSentinel) continue; return { value: record.arg, done: context.done }; } "throw" === record.type && (state = "completed", context.method = "throw", context.arg = record.arg); } }; }(innerFn, self, context), generator; } function tryCatch(fn, obj, arg) { try { return { type: "normal", arg: fn.call(obj, arg) }; } catch (err) { return { type: "throw", arg: err }; } } exports.wrap = wrap; var ContinueSentinel = {}; function Generator() {} function GeneratorFunction() {} function GeneratorFunctionPrototype() {} var IteratorPrototype = {}; define(IteratorPrototype, iteratorSymbol, function () { return this; }); var getProto = Object.getPrototypeOf, NativeIteratorPrototype = getProto && getProto(getProto(values([]))); NativeIteratorPrototype && NativeIteratorPrototype !== Op && hasOwn.call(NativeIteratorPrototype, iteratorSymbol) && (IteratorPrototype = NativeIteratorPrototype); var Gp = GeneratorFunctionPrototype.prototype = Generator.prototype = Object.create(IteratorPrototype); function defineIteratorMethods(prototype) { ["next", "throw", "return"].forEach(function (method) { define(prototype, method, function (arg) { return this._invoke(method, arg); }); }); } function AsyncIterator(generator, PromiseImpl) { function invoke(method, arg, resolve, reject) { var record = tryCatch(generator[method], generator, arg); if ("throw" !== record.type) { var result = record.arg, value = result.value; return value && "object" == typeof value && hasOwn.call(value, "__await") ? PromiseImpl.resolve(value.__await).then(function (value) { invoke("next", value, resolve, reject); }, function (err) { invoke("throw", err, resolve, reject); }) : PromiseImpl.resolve(value).then(function (unwrapped) { result.value = unwrapped, resolve(result); }, function (error) { return invoke("throw", error, resolve, reject); }); } reject(record.arg); } var previousPromise; this._invoke = function (method, arg) { function callInvokeWithMethodAndArg() { return new PromiseImpl(function (resolve, reject) { invoke(method, arg, resolve, reject); }); } return previousPromise = previousPromise ? previousPromise.then(callInvokeWithMethodAndArg, callInvokeWithMethodAndArg) : callInvokeWithMethodAndArg(); }; } function maybeInvokeDelegate(delegate, context) { var method = delegate.iterator[context.method]; if (undefined === method) { if (context.delegate = null, "throw" === context.method) { if (delegate.iterator.return && (context.method = "return", context.arg = undefined, maybeInvokeDelegate(delegate, context), "throw" === context.method)) return ContinueSentinel; context.method = "throw", context.arg = new TypeError("The iterator does not provide a 'throw' method"); } return ContinueSentinel; } var record = tryCatch(method, delegate.iterator, context.arg); if ("throw" === record.type) return context.method = "throw", context.arg = record.arg, context.delegate = null, ContinueSentinel; var info = record.arg; return info ? info.done ? (context[delegate.resultName] = info.value, context.next = delegate.nextLoc, "return" !== context.method && (context.method = "next", context.arg = undefined), context.delegate = null, ContinueSentinel) : info : (context.method = "throw", context.arg = new TypeError("iterator result is not an object"), context.delegate = null, ContinueSentinel); } function pushTryEntry(locs) { var entry = { tryLoc: locs[0] }; 1 in locs && (entry.catchLoc = locs[1]), 2 in locs && (entry.finallyLoc = locs[2], entry.afterLoc = locs[3]), this.tryEntries.push(entry); } function resetTryEntry(entry) { var record = entry.completion || {}; record.type = "normal", delete record.arg, entry.completion = record; } function Context(tryLocsList) { this.tryEntries = [{ tryLoc: "root" }], tryLocsList.forEach(pushTryEntry, this), this.reset(!0); } function values(iterable) { if (iterable) { var iteratorMethod = iterable[iteratorSymbol]; if (iteratorMethod) return iteratorMethod.call(iterable); if ("function" == typeof iterable.next) return iterable; if (!isNaN(iterable.length)) { var i = -1, next = function next() { for (; ++i < iterable.length;) if (hasOwn.call(iterable, i)) return next.value = iterable[i], next.done = !1, next; return next.value = undefined, next.done = !0, next; }; return next.next = next; } } return { next: doneResult }; } function doneResult() { return { value: undefined, done: !0 }; } return GeneratorFunction.prototype = GeneratorFunctionPrototype, define(Gp, "constructor", GeneratorFunctionPrototype), define(GeneratorFunctionPrototype, "constructor", GeneratorFunction), GeneratorFunction.displayName = define(GeneratorFunctionPrototype, toStringTagSymbol, "GeneratorFunction"), exports.isGeneratorFunction = function (genFun) { var ctor = "function" == typeof genFun && genFun.constructor; return !!ctor && (ctor === GeneratorFunction || "GeneratorFunction" === (ctor.displayName || ctor.name)); }, exports.mark = function (genFun) { return Object.setPrototypeOf ? Object.setPrototypeOf(genFun, GeneratorFunctionPrototype) : (genFun.__proto__ = GeneratorFunctionPrototype, define(genFun, toStringTagSymbol, "GeneratorFunction")), genFun.prototype = Object.create(Gp), genFun; }, exports.awrap = function (arg) { return { __await: arg }; }, defineIteratorMethods(AsyncIterator.prototype), define(AsyncIterator.prototype, asyncIteratorSymbol, function () { return this; }), exports.AsyncIterator = AsyncIterator, exports.async = function (innerFn, outerFn, self, tryLocsList, PromiseImpl) { void 0 === PromiseImpl && (PromiseImpl = Promise); var iter = new AsyncIterator(wrap(innerFn, outerFn, self, tryLocsList), PromiseImpl); return exports.isGeneratorFunction(outerFn) ? iter : iter.next().then(function (result) { return result.done ? result.value : iter.next(); }); }, defineIteratorMethods(Gp), define(Gp, toStringTagSymbol, "Generator"), define(Gp, iteratorSymbol, function () { return this; }), define(Gp, "toString", function () { return "[object Generator]"; }), exports.keys = function (object) { var keys = []; for (var key in object) keys.push(key); return keys.reverse(), function next() { for (; keys.length;) { var key = keys.pop(); if (key in object) return next.value = key, next.done = !1, next; } return next.done = !0, next; }; }, exports.values = values, Context.prototype = { constructor: Context, reset: function reset(skipTempReset) { if (this.prev = 0, this.next = 0, this.sent = this._sent = undefined, this.done = !1, this.delegate = null, this.method = "next", this.arg = undefined, this.tryEntries.forEach(resetTryEntry), !skipTempReset) for (var name in this) "t" === name.charAt(0) && hasOwn.call(this, name) && !isNaN(+name.slice(1)) && (this[name] = undefined); }, stop: function stop() { this.done = !0; var rootRecord = this.tryEntries[0].completion; if ("throw" === rootRecord.type) throw rootRecord.arg; return this.rval; }, dispatchException: function dispatchException(exception) { if (this.done) throw exception; var context = this; function handle(loc, caught) { return record.type = "throw", record.arg = exception, context.next = loc, caught && (context.method = "next", context.arg = undefined), !!caught; } for (var i = this.tryEntries.length - 1; i >= 0; --i) { var entry = this.tryEntries[i], record = entry.completion; if ("root" === entry.tryLoc) return handle("end"); if (entry.tryLoc <= this.prev) { var hasCatch = hasOwn.call(entry, "catchLoc"), hasFinally = hasOwn.call(entry, "finallyLoc"); if (hasCatch && hasFinally) { if (this.prev < entry.catchLoc) return handle(entry.catchLoc, !0); if (this.prev < entry.finallyLoc) return handle(entry.finallyLoc); } else if (hasCatch) { if (this.prev < entry.catchLoc) return handle(entry.catchLoc, !0); } else { if (!hasFinally) throw new Error("try statement without catch or finally"); if (this.prev < entry.finallyLoc) return handle(entry.finallyLoc); } } } }, abrupt: function abrupt(type, arg) { for (var i = this.tryEntries.length - 1; i >= 0; --i) { var entry = this.tryEntries[i]; if (entry.tryLoc <= this.prev && hasOwn.call(entry, "finallyLoc") && this.prev < entry.finallyLoc) { var finallyEntry = entry; break; } } finallyEntry && ("break" === type || "continue" === type) && finallyEntry.tryLoc <= arg && arg <= finallyEntry.finallyLoc && (finallyEntry = null); var record = finallyEntry ? finallyEntry.completion : {}; return record.type = type, record.arg = arg, finallyEntry ? (this.method = "next", this.next = finallyEntry.finallyLoc, ContinueSentinel) : this.complete(record); }, complete: function complete(record, afterLoc) { if ("throw" === record.type) throw record.arg; return "break" === record.type || "continue" === record.type ? this.next = record.arg : "return" === record.type ? (this.rval = this.arg = record.arg, this.method = "return", this.next = "end") : "normal" === record.type && afterLoc && (this.next = afterLoc), ContinueSentinel; }, finish: function finish(finallyLoc) { for (var i = this.tryEntries.length - 1; i >= 0; --i) { var entry = this.tryEntries[i]; if (entry.finallyLoc === finallyLoc) return this.complete(entry.completion, entry.afterLoc), resetTryEntry(entry), ContinueSentinel; } }, catch: function _catch(tryLoc) { for (var i = this.tryEntries.length - 1; i >= 0; --i) { var entry = this.tryEntries[i]; if (entry.tryLoc === tryLoc) { var record = entry.completion; if ("throw" === record.type) { var thrown = record.arg; resetTryEntry(entry); } return thrown; } } throw new Error("illegal catch attempt"); }, delegateYield: function delegateYield(iterable, resultName, nextLoc) { return this.delegate = { iterator: values(iterable), resultName: resultName, nextLoc: nextLoc }, "next" === this.method && (this.arg = undefined), ContinueSentinel; } }, exports; }

function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

import { Modal, message, Input, Upload } from 'antd';
import { DataUri } from '@antv/x6';
import { nextTick, getGraphJSONData, autoSaveJson, launchFullScreen, exitFullscreen } from './tools';
import React from 'react';
const Dragger = Upload.Dragger; // 文件解析

const fileExplain = file => {
  const reader = new FileReader();
  reader.readAsText(file);

  reader.onload = /*#__PURE__*/function () {
    var _ref = _asyncToGenerator( /*#__PURE__*/_regeneratorRuntime().mark(function _callee(event) {
      var _event$target, jsonData;

      return _regeneratorRuntime().wrap(function _callee$(_context) {
        while (1) switch (_context.prev = _context.next) {
          case 0:
            Modal.destroyAll();
            _context.prev = 1;
            jsonData = JSON.parse(((_event$target = event.target) === null || _event$target === void 0 ? void 0 : _event$target.result) || '[]');
            message.success('文件解析完成');
            _context.next = 6;
            return autoSaveJson(jsonData, null);

          case 6:
            window.location.reload();
            _context.next = 12;
            break;

          case 9:
            _context.prev = 9;
            _context.t0 = _context["catch"](1);
            message.error('文件解析失败');

          case 12:
          case "end":
            return _context.stop();
        }
      }, _callee, null, [[1, 9]]);
    }));

    return function (_x) {
      return _ref.apply(this, arguments);
    };
  }();

  Modal.info({
    closable: false,
    className: 'rasir-fc-model',
    title: '文件解析',
    content: '文件解析中，请稍等...',
    okButtonProps: {
      disabled: true
    }
  });
};

export default function useHooks({
  onRemoveLink,
  onRemoveNode,
  changeCells,
  onAddLink,
  onSave,
  defaultEdgeShape
}) {
  const actionFunMap = {
    // 编辑节点
    edite: (_, __, config) => {
      const setVisible = config.setVisible;
      setVisible(true);
    },
    // 删除节点或连线
    delete: (cell, graph) => {
      let cellItems = [];
      if (cell) cellItems = [cell];else {
        // 如果currentCell没有值 就去graph中找有没有被选中的节点
        cellItems = graph.getSelectedCells();
      }

      if (!cellItems.length) {
        message.info('请先选择要删除的节点', {
          duration: 1000
        });
        return;
      }

      const isNode = cellItems[0].isNode();
      const isEdge = cellItems[0].isEdge(); // 删除节点

      if (isNode) {
        Modal.confirm({
          title: '删除节点',
          className: 'rasir-fc-model',
          content: `该操作将删除节点 ${cellItems.map(cell => cell.label).join(',')} 是否继续？`,
          okText: '确定',
          cancelText: '取消',
          centered: true,
          onOk: () => new Promise( /*#__PURE__*/function () {
            var _ref2 = _asyncToGenerator( /*#__PURE__*/_regeneratorRuntime().mark(function _callee2(resolve, reject) {
              return _regeneratorRuntime().wrap(function _callee2$(_context2) {
                while (1) switch (_context2.prev = _context2.next) {
                  case 0:
                    _context2.t0 = !onRemoveNode;

                    if (_context2.t0) {
                      _context2.next = 8;
                      break;
                    }

                    _context2.t1 = onRemoveNode;

                    if (!_context2.t1) {
                      _context2.next = 7;
                      break;
                    }

                    _context2.next = 6;
                    return onRemoveNode(cellItems.map(cell => cell.data));

                  case 6:
                    _context2.t1 = _context2.sent;

                  case 7:
                    _context2.t0 = _context2.t1;

                  case 8:
                    if (!_context2.t0) {
                      _context2.next = 14;
                      break;
                    }

                    graph.removeCells(cellItems.map(cell => graph.getCellById(cell.data.id)));
                    resolve(1);
                    changeCells();
                    _context2.next = 15;
                    break;

                  case 14:
                    reject();

                  case 15:
                  case "end":
                    return _context2.stop();
                }
              }, _callee2);
            }));

            return function (_x2, _x3) {
              return _ref2.apply(this, arguments);
            };
          }())
        });
      } else if (isEdge) {
        // 删除连线
        Modal.confirm({
          title: '删除连线',
          className: 'rasir-fc-model',
          content: `该操作将删除连线是否继续？请确认`,
          okText: '确定',
          cancelText: '取消',
          centered: true,
          onOk: () => new Promise( /*#__PURE__*/function () {
            var _ref3 = _asyncToGenerator( /*#__PURE__*/_regeneratorRuntime().mark(function _callee3(resolve, reject) {
              return _regeneratorRuntime().wrap(function _callee3$(_context3) {
                while (1) switch (_context3.prev = _context3.next) {
                  case 0:
                    _context3.t0 = onRemoveLink;

                    if (!_context3.t0) {
                      _context3.next = 5;
                      break;
                    }

                    _context3.next = 4;
                    return onRemoveLink(cellItems);

                  case 4:
                    _context3.t0 = !_context3.sent;

                  case 5:
                    if (!_context3.t0) {
                      _context3.next = 9;
                      break;
                    }

                    reject();
                    _context3.next = 12;
                    break;

                  case 9:
                    graph.removeCells(cellItems);
                    resolve(1);
                    changeCells();

                  case 12:
                  case "end":
                    return _context3.stop();
                }
              }, _callee3);
            }));

            return function (_x4, _x5) {
              return _ref3.apply(this, arguments);
            };
          }()),

          onCancel() {
            cellItems.forEach(cellItem => {
              graph.addCell(cellItem);
            });
          }

        });
      }
    },
    // 添加节点或连线
    add: function () {
      var _add = _asyncToGenerator( /*#__PURE__*/_regeneratorRuntime().mark(function _callee4(cellItem, graph) {
        var isEdge, _ref4, source, target, edgeData;

        return _regeneratorRuntime().wrap(function _callee4$(_context4) {
          while (1) switch (_context4.prev = _context4.next) {
            case 0:
              isEdge = cellItem.isEdge();

              if (!isEdge) {
                _context4.next = 15;
                break;
              }

              message.loading({
                duration: 0,
                content: '连接生成中，请稍等'
              });
              _context4.t0 = onAddLink;

              if (!_context4.t0) {
                _context4.next = 8;
                break;
              }

              _context4.next = 7;
              return onAddLink();

            case 7:
              _context4.t0 = !_context4.sent;

            case 8:
              if (_context4.t0) {
                _context4.next = 15;
                break;
              }

              _ref4 = cellItem, source = _ref4.source, target = _ref4.target;
              edgeData = {
                id: `${source.cell}_${target.cell}`,
                source: source.cell,
                target: target.cell
              };
              graph.addEdge(_objectSpread({
                shape: defaultEdgeShape,
                data: _objectSpread(_objectSpread({}, edgeData), {}, {
                  type: 'link',
                  zIndex: 1
                })
              }, edgeData));
              message.destroy();
              message.success('连接成功');
              changeCells();

            case 15:
            case "end":
              return _context4.stop();
          }
        }, _callee4);
      }));

      function add(_x6, _x7) {
        return _add.apply(this, arguments);
      }

      return add;
    }(),
    // 缩小
    smaller: (_, __, {
      zoom,
      setZoom
    }) => {
      if (zoom <= 0.5) return;
      setZoom(zoom - 0.1);
    },
    // 放大
    bigger: (_, __, {
      zoom,
      setZoom
    }) => {
      if (zoom >= 1.5) return;
      setZoom(zoom + 0.1);
    },
    // 复制节点
    copy: (_, graph) => {
      const cellItems = graph.getSelectedCells();
      if (!cellItems.length) return message.info('请先选择要复制的节点');
      if (cellItems.length > 1) return message.info('一次只能复制一个节点');
      graph.copy(cellItems);
      message.success('节点成功放入剪贴板');
    },
    // 粘贴节点
    paste: (_, graph, {
      setVisible,
      setCurrentCell
    }) => {
      if (!graph.isClipboardEmpty()) {
        const _graph$getCellsInClip = graph.getCellsInClipboard(),
              _graph$getCellsInClip2 = _slicedToArray(_graph$getCellsInClip, 1),
              cell = _graph$getCellsInClip2[0];

        graph.cleanSelection();
        cell.setData({
          id: null
        }, {
          deep: true
        });
        setCurrentCell(cell);
        nextTick(() => setVisible(true));
      } else {
        message.info('剪贴板中没有节点');
      }
    },
    // 导出图片
    exportImg: (_, graph) => {
      let chartName = undefined;
      Modal.confirm({
        title: '导出图片',
        className: 'rasir-fc-model',
        cancelText: '取消',
        okText: '确定',
        width: 500,
        onOk: () => new Promise((resolve, reject) => {
          if (!chartName) {
            message.info('请输入图片名称');
            reject();
          } else {
            graph.toPNG(dataUri => {
              // 下载
              DataUri.downloadDataUri(dataUri, `${chartName}.png`);
              resolve(1);
            }, {
              quality: 1,
              padding: {
                top: 20,
                right: 20,
                bottom: 20,
                left: 20
              }
            });
          }
        }),
        content: /*#__PURE__*/React.createElement(Input, {
          value: chartName,
          onChange: e => chartName = e.target.value,
          placeholder: "\u8BF7\u8F93\u5165\u56FE\u7247\u540D\u79F0"
        })
      });
    },
    // 保存到后端
    save: (_, graph) => {
      if (!onSave) return;
      Modal.confirm({
        title: '数据上传',
        className: 'rasir-fc-model',
        content: `该操作将数据上传到云端，请确认`,
        okText: '确定',
        cancelText: '取消',
        centered: true,
        onOk: () => new Promise( /*#__PURE__*/function () {
          var _ref5 = _asyncToGenerator( /*#__PURE__*/_regeneratorRuntime().mark(function _callee5(resolve, reject) {
            var ret;
            return _regeneratorRuntime().wrap(function _callee5$(_context5) {
              while (1) switch (_context5.prev = _context5.next) {
                case 0:
                  _context5.next = 2;
                  return onSave(getGraphJSONData(graph));

                case 2:
                  ret = _context5.sent;

                  if (ret) {
                    message.success('上传成功！');
                    resolve(1);
                  } else {
                    reject();
                  }

                case 4:
                case "end":
                  return _context5.stop();
              }
            }, _callee5);
          }));

          return function (_x8, _x9) {
            return _ref5.apply(this, arguments);
          };
        }())
      });
    },
    // 导出数据文件
    exportData: (_, graph) => {
      let fileName = '';
      Modal.confirm({
        title: '导出文件',
        className: 'rasir-fc-model',
        content: /*#__PURE__*/React.createElement("div", null, /*#__PURE__*/React.createElement(Input, {
          onChange: e => fileName = e.target.value,
          placeholder: "\u8BF7\u8F93\u5165\u6587\u4EF6\u540D\u79F0"
        })),
        okText: '确定',
        cancelText: '取消',
        onOk: () => {
          if (!fileName) {
            message.error('请输入文件名称');
            return Promise.reject();
          }

          try {
            const content = JSON.stringify(getGraphJSONData(graph));
            const file = new Blob([content], {
              type: 'application/json'
            });
            const fileUrl = URL.createObjectURL(file);
            const linkElement = document.createElement('a');
            linkElement.setAttribute('href', fileUrl);
            linkElement.setAttribute('download', `${fileName}.json`);
            linkElement.click();
            return Promise.resolve(1);
          } catch (e) {
            message.error('数据导出失败');
            return Promise.resolve(1);
          }
        }
      });
    },
    // 导入文件数据
    importData: _ => {
      Modal.confirm({
        title: '导入数据',
        okText: '确定',
        cancelText: '取消',
        className: 'rasir-fc-model',
        content: /*#__PURE__*/React.createElement(Dragger, {
          action: "/",
          fileList: [],
          beforeUpload: file => {
            Modal.destroyAll();
            fileExplain(file);
            return Promise.reject();
          }
        }, /*#__PURE__*/React.createElement("p", {
          style: {
            fontSize: 40
          }
        }, "+"), /*#__PURE__*/React.createElement("p", null, "\u8BF7\u9009\u62E9\u8981\u5BFC\u5165\u7684\u6570\u636E"))
      });
    },
    // 清理缓存数据
    clearStorage: _ => {
      Modal.confirm({
        title: '清理缓存',
        className: 'rasir-fc-model',
        content: `该操作将清理缓存数据，请确认`,
        okText: '确定',
        cancelText: '取消',
        centered: true,
        onOk: function () {
          var _onOk = _asyncToGenerator( /*#__PURE__*/_regeneratorRuntime().mark(function _callee6() {
            return _regeneratorRuntime().wrap(function _callee6$(_context6) {
              while (1) switch (_context6.prev = _context6.next) {
                case 0:
                  _context6.next = 2;
                  return autoSaveJson(null, null);

                case 2:
                  window.location.reload();

                case 3:
                case "end":
                  return _context6.stop();
              }
            }, _callee6);
          }));

          function onOk() {
            return _onOk.apply(this, arguments);
          }

          return onOk;
        }()
      });
    },
    // 全屏
    full: (_, __, {
      fullContainer,
      changeFullScreeen
    }) => {
      launchFullScreen(fullContainer);
      nextTick(() => changeFullScreeen(true));
    },
    // 取消全屏
    normal: (_, __, {
      changeFullScreeen
    }) => {
      exitFullscreen();
      nextTick(() => changeFullScreeen(false));
    },
    // 撤销
    undo: (_, graph) => {
      const history = graph.history;
      history.undo();
    },
    // 重做
    redo: (_, graph) => {
      const history = graph.history;
      history.redo();
    }
  };
  return {
    actionFunMap
  };
}