declare module '*.css';
declare module '*.less';
declare module '*.png';
declare module '*.jpg';
declare module '*.svg';

declare interface window {
  _saveInterval?: number;
  _globalAssets?: any;
  _useCache?: boolean;
  _moduleProps?: any;
}
