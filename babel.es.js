module.exports = {
  'presets': [
    [
      '@babel/preset-env',
      {
        modules: false,
        targets: {
          'chrome': '49',
        }
      }
    ],
    '@babel/preset-react',
    '@babel/preset-typescript'
  ]
};
