import React, { useEffect, useState } from 'react';
import { Button, Tooltip } from 'antd';
import { ActionTypes } from '../../types';
import Icon from '../Icon';

interface ActionButtonsProps {
  onActions: (action: string) => void;
  isFullScreen?: boolean;
  canRedo?: boolean;
  canUndo?: boolean;
  showActions?: boolean;
  actions?: ActionTypes[];
  appendActions?: React.ReactNode;
}

const defaultActions = (state: boolean = true) => ({
  reUnDo: state,
  zoom: state,
  importData: state,
  exportData: state,
  exportImg: state,
  submit: state,
  copy: state,
  paste: state,
  del: state,
  clear: state,
  plusActions: state,
});
const ActionButtons: React.FC<ActionButtonsProps> = function ({
  onActions,
  isFullScreen = false,
  canRedo = false,
  canUndo = false,
  showActions = true,
  actions,
  appendActions,
}) {
  const [showState, setShowState] = useState(defaultActions());

  useEffect(() => {
    if (Array.isArray(actions)) {
      const state = defaultActions(false);
      state.plusActions = true;
      actions.forEach((key) => {
        switch (key) {
          case 'RE_UN_DO':
            state.reUnDo = true;
            break;
          case 'ZOOM':
            state.zoom = true;
            break;
          case 'IMPORT_DATA':
            state.importData = true;
            state.plusActions = true;
            break;
          case 'EXPORT_DATA':
            state.exportData = true;
            state.plusActions = true;
            break;
          case 'EXPORT_IMG':
            state.exportImg = true;
            state.plusActions = true;
            break;
          case 'COPY':
            state.copy = true;
            state.plusActions = true;
            break;
          case 'PASTE':
            state.paste = true;
            state.plusActions = true;
            break;
          case 'SUBMIT':
            state.submit = true;
            state.plusActions = true;
            break;
          case 'DELETE':
            state.del = true;
            state.plusActions = true;
            break;
          case 'CLEAR':
            state.clear = true;
            state.plusActions = true;
            break;
          default:
            break;
        }
      });
      setShowState(state);
    } else {
      setShowState(defaultActions());
    }
  }, [actions]);

  const {
    reUnDo,
    zoom,
    importData,
    exportData,
    exportImg,
    submit,
    copy,
    paste,
    clear,
    del,
    plusActions,
  } = showState;

  return (
    <div className="ra-arch-actionWrapper">
      {showActions && (
        <div className="ra-arch-actionButtons">
          {reUnDo && (
            <div className="ra-arch-btns-group">
              <Tooltip title="撤销">
                <Button disabled={!canUndo} onClick={() => onActions('undo')}>
                  <Icon type="icon-undo" />
                </Button>
              </Tooltip>
              <Tooltip title="重做">
                <Button disabled={!canRedo} onClick={() => onActions('redo')}>
                  <Icon type="icon-redo" />
                </Button>
              </Tooltip>
            </div>
          )}
          {zoom && (
            <div className="ra-arch-btns-group">
              <Tooltip title="缩小">
                <Button onClick={() => onActions('smaller')}>
                  <Icon type="icon-smaller" />
                </Button>
              </Tooltip>
              <Tooltip title="放大">
                <Button onClick={() => onActions('bigger')}>
                  <Icon type="icon-bigger" />
                </Button>
              </Tooltip>
              {isFullScreen ? (
                <Tooltip title="还原">
                  <Button onClick={() => onActions('normal')}>
                    <Icon type="icon-small" />
                  </Button>
                </Tooltip>
              ) : (
                <Tooltip title="全屏">
                  <Button onClick={() => onActions('full')}>
                    <Icon type="icon-full" />
                  </Button>
                </Tooltip>
              )}
            </div>
          )}
          {plusActions && (
            <div className="ra-arch-btns-group">
              {importData && (
                <Tooltip title="本地导入数据">
                  <Button onClick={() => onActions('importData')}>
                    <Icon type="icon-import-data" />
                  </Button>
                </Tooltip>
              )}
              {exportData && (
                <Tooltip title="本地导出数据">
                  <Button onClick={() => onActions('exportData')}>
                    <Icon type="icon-export-data" />
                  </Button>
                </Tooltip>
              )}
              {exportImg && (
                <Tooltip title="本地导出图片">
                  <Button onClick={() => onActions('exportImg')}>
                    <Icon type="icon-export-img" />
                  </Button>
                </Tooltip>
              )}
              {submit && (
                <Tooltip title="上传数据">
                  <Button onClick={() => onActions('save')}>
                    <Icon type="icon-save-clound" />
                  </Button>
                </Tooltip>
              )}
              {copy && (
                <Tooltip title="复制节点">
                  <Button onClick={() => onActions('copy')}>
                    <Icon type="icon-copy" />
                  </Button>
                </Tooltip>
              )}
              {paste && (
                <Tooltip title="粘贴节点">
                  <Button onClick={() => onActions('paste')}>
                    <Icon type="icon-paste" />
                  </Button>
                </Tooltip>
              )}
              {del && (
                <Tooltip title="删除节点">
                  <Button onClick={() => onActions('delete')}>
                    <Icon type="icon-delete" />
                  </Button>
                </Tooltip>
              )}
              {clear && (
                <Tooltip title="清理缓存">
                  <Button onClick={() => onActions('clearStorage')}>
                    <Icon type="icon-clear-storage" />
                  </Button>
                </Tooltip>
              )}
            </div>
          )}
        </div>
      )}
      <div className="ra-arch-systemDesc">
        {appendActions}
        {/* <div>
          <img src={innerSystem} />
          内部系统
        </div>
        <div>
          <img src={outerSystem} />
          外部系统
        </div> */}
      </div>
    </div>
  );
};

export default ActionButtons;
