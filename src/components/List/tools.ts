import { Graph, Node, Cell } from '@antv/x6';
import { getNodeAttr } from '../Lane/tools';

export function getConfig(
  target: Graph,
  configs: any,
  assets: any,
  onNodeCreate: (node: Node) => void,
  defaultNodeShape: string,
): any {
  const { title, search, options } = configs;
  return {
    title,
    target,
    animation: false,
    validateNode: (node: Node) => {
      const { x, y } = node.position();
      const { width: w, height: h } = node.size();
      const [px, py] = [x + w / 2, y + h / 2];
      const cells = target.getNodesFromPoint({ x: px, y: py });
      const preId = cells.find((cell) => cell.data.type === 'lane')?.id;
      if (preId && onNodeCreate) {
        node.data.preId = preId;
        onNodeCreate(node);
      }
      return false;
    },
    getDragNode: (node: any) => {
      return target.createNode({
        shape: defaultNodeShape,
        label: node.label,
        data: node.data,
        type: 'node',
        attrs: {
          body: {
            fill: '#ccc',
          },
          ...getNodeAttr(defaultNodeShape, node.data, assets),
        },
      });
    },
    search: (cell: Cell, keyword: string) => {
      if (keyword) {
        return (
          cell.shape === 'moduleNode' &&
          `${cell.attr('text/text')}`
            .toLowerCase()
            .includes(`${keyword}`.toLowerCase())
        );
      }
      return true;
    },
    placeholder: search?.placeholder || '请输入组件名称',
    notFoundText: '没有匹配的模型',
    stencilGraphWidth: 200,
    collapsable: false,
    groups: options.map(({ label, value, children }: any) => ({
      title: label,
      name: value,
      collapsable: false,
      graphPadding: 0,
      graphHeight: (Array.isArray(children) ? children.length * 32 : 0) + 10,
      layoutOptions: {
        columns: 1,
        columnWidth: 190,
        rowHeight: 32,
        dx: 5,
        dy: 5,
      },
    })),
  };
}

export function appendOptions(
  graph: Graph,
  stencil: any,
  options: any[],
  assets: any,
  moduleNodeShape: string,
) {
  const moduleProps: any = {};
  options.forEach((group) => {
    const { value: groupName, children } = group;
    if (!Array.isArray(children)) return;
    const items = children.map((item) => {
      const { label, value, moduleType, moduleProp } = item;
      moduleProps[moduleType || value] = moduleProp;
      item.moduleType = moduleType || value;
      let attrs = {};
      attrs = getNodeAttr(moduleNodeShape, item, assets);
      const node = graph.createNode({
        shape: 'moduleNode',
        label,
        data: item,
        attrs,
      });

      return node;
    });
    stencil.load(items, groupName);
  });
  (window as any)._moduleProps = moduleProps;
}
