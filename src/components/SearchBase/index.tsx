// 基础搜索组件通过简单配置生成搜索表单
import React from 'react';
import { Input, Form, Select, DatePicker } from 'antd';
import RemoteSelect from './RemoteSelect';
import { FormInstance } from 'antd/lib/form/Form';
import { FormItemProps } from 'antd/lib/form/FormItem';
import { SelectOption } from '../../types';

const { RangePicker } = DatePicker;
export type ItemProps = FormItemProps & { type: string; inputProps?: any };

interface SearchBaseProps {
  items: ItemProps[];
  form: FormInstance;
}

export const BaseItem: React.FC<any> = function ({ type, options, ...props }) {
  if (!props.style) props.style = {};
  if (!props.style.width) {
    props.style.width = '100%';
  }
  return (
    <>
      {type === 'INPUT' && <Input {...props} />}
      {type === 'TEXTAREA' && <Input.TextArea {...props} />}
      {type === 'SEARCH' && <Input.Search {...props} />}
      {type === 'REMOTESELECT' && <RemoteSelect {...props} />}
      {type === 'SELECT' && (
        <Select {...props}>
          {options?.map((e: SelectOption) => (
            <Select.Option key={e.value as string} value={e.value as string}>
              {e.label}
            </Select.Option>
          ))}
        </Select>
      )}
      {type === 'DATETIMERANGER' && <RangePicker showTime {...props} />}
      {type === 'DATERANGER' && <RangePicker {...props} />}
    </>
  );
};

const formLayout = {
  wrapperCol: {
    span: 16,
  },
  labelCol: {
    span: 8,
  },
};

const SearchBase: React.FC<SearchBaseProps> = function ({ items = [], form }) {
  return (
    <Form
      {...formLayout}
      className="ra-arch-searchBase"
      style={{ background: '#fff' }}
      form={form}
    >
      {items.map(({ type, inputProps, ...props }, index) => (
        <Form.Item key={`${index + 1}`} {...props}>
          <BaseItem type={type} {...inputProps} />
        </Form.Item>
      ))}
    </Form>
  );
};

export default SearchBase;
