// iconfont 图标组件
import React from 'react';
// import './index.less';

const Icon = function ({ type, className = '', color, ...prpos }: any) {
  return (
    <span>
      <svg
        className={`ra-icon ${className}`}
        aria-hidden="true"
        style={{ color }}
        {...prpos}
      >
        <use xlinkHref={`#${type}`}></use>
      </svg>
    </span>
  );
};

export default Icon;
