import { FormInstance, FormRule, FormItemProps } from 'antd';
import { Node } from '@antv/x6';
import React from 'react';

export type SizeType = 'small' | 'middle' | 'large' | undefined;

export interface SelectOption {
  label: string | React.ReactNode;
  value: string | number | boolean;
  key?: string | number | boolean;
  [key: string]: any;
}

export interface IndexStoreData {
  id: string;
  updateTime: number;
  data: undefined | ArchData;
  expired: number;
}

export interface Point {
  x: number;
  y: number;
}

export interface LinkData {
  source: string | number;
  target: string | number;
  zIndex?: number;
  data?: KeyValue;
  shape?: string;
  [key: string]: any;
}

export interface KeyValue {
  [key: string]: any;
}

export type NodeData = {
  id: string;
  name?: string;
  type?: string;
  width?: number;
  height?: number;
  zIndex?: number;
  x?: number;
  y?: number;
  moduleType?: string;
  preId?: string;
  shape?: string;
  [key: string]: any;
};

export type ArchData = {
  width?: number;
  height?: number;
  lanes: NodeData[];
  nodes: NodeData[];
  links: LinkData[];
};
export type GraphData = {
  width?: number;
  height?: number;
  laneList: Node.Metadata[];
  nodeList: Node.Metadata[];
  linkList: LinkData[];
};

export type InputProp = {
  placeholder?: string;
  options?: SelectOption[];
  allowClear?: boolean;
  className?: string;
  size?: SizeType;
  [key: string]: any;
};

export type InputType =
  | 'INPUT'
  | 'SEARCH'
  | 'REMOTESELECT'
  | 'SELECT'
  | 'DATETIMERANGER'
  | 'DATERANGER'
  | 'TEXTAREA';

export type ModuleFormItem = Omit<FormItemProps, 'children'> & {
  type: InputType;
  inputProps?: InputProp;
  rules?: FormRule[];
};

export interface ModuleItemChildren {
  label: string;
  value: string;
  target?: string[];
  icon?: string;
  moduleProp?:
    | ModuleFormItem[]
    | ((data: any, form: FormInstance) => ModuleFormItem[])
    | ((data: any, form: FormInstance) => React.ReactNode);
  [key: string]: any;
}

export interface ModuleItem {
  label: string | React.ReactNode;
  value: string | number;
  children: ModuleItemChildren[];
  [key: string]: any;
}

export interface ModuleConfig {
  title?: string;
  search?: {
    placeholder?: string;
  };
  options: ModuleItem[];
}

export type ActionTypes =
  | 'RE_UN_DO'
  | 'ZOOM'
  | 'EXPORT_DATA'
  | 'IMPORT_DATA'
  | 'EXPORT_IMG'
  | 'SUBMIT'
  | 'COPY'
  | 'PASTE'
  | 'DELETE'
  | 'CLEAR';

export type Register = {
  name: string;
  options: any;
  type: 'NODE' | 'EDGE';
  handler?: NodeHanlder;
};

export type NodeHanlder = (data: KeyValue, assets: KeyValue) => KeyValue;
