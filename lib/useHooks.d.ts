export default function useHooks({ onRemoveLink, onRemoveNode, changeCells, onAddLink, onSave, defaultEdgeShape, }: any): {
    actionFunMap: {
        [key: string]: (...args: any) => void;
    };
};
