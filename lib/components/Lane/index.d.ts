import React from 'react';
import { Graph } from '@antv/x6';
import { GraphData } from '../../types';
interface LaneProps {
    data: GraphData;
    zoom?: number;
    isFullScreen?: boolean;
    onActions?: (action: string) => void;
    onLoad: (graph: any) => void;
    assets: {
        [key: string]: string;
    };
    setCurrentCell: (cell: any) => void;
    setCanRedo: (canRedo: boolean) => void;
    setCanUndo: (canUndo: boolean) => void;
    onUndoOrRedo?: (type: 'undo' | 'redo', graph: Graph, args: {
        cmds: any[];
        options: any;
    }) => void;
    defaultEdgeShape?: string;
}
declare const _default: React.NamedExoticComponent<LaneProps>;
export default _default;
