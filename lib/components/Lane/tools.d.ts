import { Graph, Cell, Node } from '@antv/x6';
import { Register } from '../../types';
export declare function getNodeAttr(shape: string | undefined, data: any, assets: any): import("../../types").KeyValue;
export declare function registerAll(list?: Register[]): void;
export declare function registerModuleNode(): void;
export declare function registerLane(config?: {
    width?: number;
    height?: number;
}): void;
export declare function registerEdge(): void;
export declare function registerDragGraph(graph: Graph, xAxisRef: any, yAxisRef: any): () => void;
export declare function registerResizeLane(graph: Graph, onLaneResize: (dragNode: Node, width: number, height: number, minHeight: number) => void): void;
export declare function registerDbClickNode(graph: Graph, setCurrentCell: (node: Cell) => void, handlerActions?: (key: string) => void): void;
export declare function registerResizeNode(graph: Graph): void;
export declare function registerNodeMenuContext(graph: Graph, menuRef: any, callback?: (node: any) => void): void;
export declare function registerAutoSave(graph: Graph, callback?: (data: any) => void): void;
export declare function registerEdgeRemove(graph: Graph, callback?: (edge: any) => void): void;
export declare function registerNodeNumChange(graph: Graph): void;
export declare function registerHistory(graph: Graph, configs: {
    setCanRedo: (canRedo: boolean) => void;
    setCanUndo: (canUndo: boolean) => void;
    onUndoOrRedo?: (type: 'undo' | 'redo', graph: Graph, args: {
        cmds: any[];
        options: any;
    }) => void;
}): void;
export declare function registerLaneCellChange(cell: Cell, onLaneSizeChanged: () => void): void;
export declare function registerKeyboard(graph: Graph, onActions?: (action: string) => void): void;
export declare function getGraph(container: HTMLDivElement, minimapContainer: HTMLDivElement | null | undefined, config?: any): Graph;
