"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _react = _interopRequireWildcard(require("react"));

var _antd = require("antd");

var _SearchBase = _interopRequireDefault(require("../SearchBase"));

var _tools = require("../../tools");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _getRequireWildcardCache(nodeInterop) { if (typeof WeakMap !== "function") return null; var cacheBabelInterop = new WeakMap(); var cacheNodeInterop = new WeakMap(); return (_getRequireWildcardCache = function _getRequireWildcardCache(nodeInterop) { return nodeInterop ? cacheNodeInterop : cacheBabelInterop; })(nodeInterop); }

function _interopRequireWildcard(obj, nodeInterop) { if (!nodeInterop && obj && obj.__esModule) { return obj; } if (obj === null || typeof obj !== "object" && typeof obj !== "function") { return { default: obj }; } var cache = _getRequireWildcardCache(nodeInterop); if (cache && cache.has(obj)) { return cache.get(obj); } var newObj = {}; var hasPropertyDescriptor = Object.defineProperty && Object.getOwnPropertyDescriptor; for (var key in obj) { if (key !== "default" && Object.prototype.hasOwnProperty.call(obj, key)) { var desc = hasPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : null; if (desc && (desc.get || desc.set)) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } newObj.default = obj; if (cache) { cache.set(obj, newObj); } return newObj; }

function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

var CreateNode = function CreateNode(_ref) {
  var _node$data;

  var {
    visible,
    onClose,
    onSubmit,
    onLoad,
    node,
    rootNode
  } = _ref;

  var [form] = _antd.Form.useForm();

  var [items, setItems] = (0, _react.useState)([]);
  var [loading, setLoading] = (0, _react.useState)(false);
  var [FormItems, setFormItems] = (0, _react.useState)();
  (0, _react.useEffect)(() => {
    onLoad && onLoad(form);
  }, []);
  (0, _react.useEffect)(() => {
    if (!visible) return;
    setFormItems(undefined);
    setItems([]);

    if (node) {
      var {
        data
      } = node;
      var moduleProps = window._moduleProps || {};
      var {
        moduleType
      } = data;
      var moduleFormItems = moduleProps[moduleType];
      var modulePropItems;

      if (Object.prototype.toString.apply(moduleFormItems) === '[object Array]') {
        modulePropItems = moduleFormItems;
      } else if (Object.prototype.toString.apply(moduleFormItems) === '[object Function]') {
        var formItemResult = moduleFormItems(data, form);

        if (Object.prototype.toString.apply(formItemResult) === '[object Array]') {
          modulePropItems = formItemResult;
        } else if (Object.prototype.toString.apply(formItemResult) === '[object Object]') {
          setFormItems(formItemResult);
        }
      }

      if (modulePropItems) {
        var initValue = modulePropItems.reduce((p, c) => {
          p[c.name] = data[c.name];
          return p;
        }, {});
        setItems(modulePropItems);
        (0, _tools.nextTick)(() => form.setFieldsValue(initValue));
      }
    }
  }, [visible, node]);

  var submitForm = () => {
    form.validateFields().then( /*#__PURE__*/function () {
      var _ref2 = _asyncToGenerator(function* (values) {
        setLoading(true);

        if (onSubmit) {
          var copyNode = node.clone();
          copyNode.setData(values);
          var ret = yield onSubmit(copyNode);
          setLoading(false);
          if (ret) onClose();
        } else {
          setLoading(false);
          onClose();
        }
      });

      return function (_x) {
        return _ref2.apply(this, arguments);
      };
    }());
  };

  return /*#__PURE__*/_react.default.createElement(_antd.Drawer, {
    title: "".concat(node !== null && node !== void 0 && (_node$data = node.data) !== null && _node$data !== void 0 && _node$data.id ? '编辑' : '新建', "\u8282\u70B9"),
    placement: "right",
    className: "ra-arch-createNode",
    closable: false,
    onClose: onClose,
    visible: visible,
    getContainer: rootNode,
    extra: /*#__PURE__*/_react.default.createElement(_antd.Button, {
      loading: loading,
      type: "primary",
      ghost: true,
      size: "small",
      onClick: submitForm
    }, "\u63D0\u4EA4")
  }, FormItems ? FormItems : /*#__PURE__*/_react.default.createElement(_SearchBase.default, {
    form: form,
    items: items
  }));
};

var _default = CreateNode;
exports.default = _default;