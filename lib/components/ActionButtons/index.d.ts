import React from 'react';
import { ActionTypes } from '../../types';
interface ActionButtonsProps {
    onActions: (action: string) => void;
    isFullScreen?: boolean;
    canRedo?: boolean;
    canUndo?: boolean;
    showActions?: boolean;
    actions?: ActionTypes[];
    appendActions?: React.ReactNode;
}
declare const ActionButtons: React.FC<ActionButtonsProps>;
export default ActionButtons;
