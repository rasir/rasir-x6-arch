"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _react = _interopRequireWildcard(require("react"));

var _antd = require("antd");

var _Icon = _interopRequireDefault(require("../Icon"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _getRequireWildcardCache(nodeInterop) { if (typeof WeakMap !== "function") return null; var cacheBabelInterop = new WeakMap(); var cacheNodeInterop = new WeakMap(); return (_getRequireWildcardCache = function _getRequireWildcardCache(nodeInterop) { return nodeInterop ? cacheNodeInterop : cacheBabelInterop; })(nodeInterop); }

function _interopRequireWildcard(obj, nodeInterop) { if (!nodeInterop && obj && obj.__esModule) { return obj; } if (obj === null || typeof obj !== "object" && typeof obj !== "function") { return { default: obj }; } var cache = _getRequireWildcardCache(nodeInterop); if (cache && cache.has(obj)) { return cache.get(obj); } var newObj = {}; var hasPropertyDescriptor = Object.defineProperty && Object.getOwnPropertyDescriptor; for (var key in obj) { if (key !== "default" && Object.prototype.hasOwnProperty.call(obj, key)) { var desc = hasPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : null; if (desc && (desc.get || desc.set)) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } newObj.default = obj; if (cache) { cache.set(obj, newObj); } return newObj; }

var defaultActions = function defaultActions() {
  var state = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : true;
  return {
    reUnDo: state,
    zoom: state,
    importData: state,
    exportData: state,
    exportImg: state,
    submit: state,
    copy: state,
    paste: state,
    del: state,
    clear: state,
    plusActions: state
  };
};

var ActionButtons = function ActionButtons(_ref) {
  var {
    onActions,
    isFullScreen = false,
    canRedo = false,
    canUndo = false,
    showActions = true,
    actions,
    appendActions
  } = _ref;
  var [showState, setShowState] = (0, _react.useState)(defaultActions());
  (0, _react.useEffect)(() => {
    if (Array.isArray(actions)) {
      var state = defaultActions(false);
      state.plusActions = true;
      actions.forEach(key => {
        switch (key) {
          case 'RE_UN_DO':
            state.reUnDo = true;
            break;

          case 'ZOOM':
            state.zoom = true;
            break;

          case 'IMPORT_DATA':
            state.importData = true;
            state.plusActions = true;
            break;

          case 'EXPORT_DATA':
            state.exportData = true;
            state.plusActions = true;
            break;

          case 'EXPORT_IMG':
            state.exportImg = true;
            state.plusActions = true;
            break;

          case 'COPY':
            state.copy = true;
            state.plusActions = true;
            break;

          case 'PASTE':
            state.paste = true;
            state.plusActions = true;
            break;

          case 'SUBMIT':
            state.submit = true;
            state.plusActions = true;
            break;

          case 'DELETE':
            state.del = true;
            state.plusActions = true;
            break;

          case 'CLEAR':
            state.clear = true;
            state.plusActions = true;
            break;

          default:
            break;
        }
      });
      setShowState(state);
    } else {
      setShowState(defaultActions());
    }
  }, [actions]);
  var {
    reUnDo,
    zoom,
    importData,
    exportData,
    exportImg,
    submit,
    copy,
    paste,
    clear,
    del,
    plusActions
  } = showState;
  return /*#__PURE__*/_react.default.createElement("div", {
    className: "ra-arch-actionWrapper"
  }, showActions && /*#__PURE__*/_react.default.createElement("div", {
    className: "ra-arch-actionButtons"
  }, reUnDo && /*#__PURE__*/_react.default.createElement("div", {
    className: "ra-arch-btns-group"
  }, /*#__PURE__*/_react.default.createElement(_antd.Tooltip, {
    title: "\u64A4\u9500"
  }, /*#__PURE__*/_react.default.createElement(_antd.Button, {
    disabled: !canUndo,
    onClick: () => onActions('undo')
  }, /*#__PURE__*/_react.default.createElement(_Icon.default, {
    type: "icon-undo"
  }))), /*#__PURE__*/_react.default.createElement(_antd.Tooltip, {
    title: "\u91CD\u505A"
  }, /*#__PURE__*/_react.default.createElement(_antd.Button, {
    disabled: !canRedo,
    onClick: () => onActions('redo')
  }, /*#__PURE__*/_react.default.createElement(_Icon.default, {
    type: "icon-redo"
  })))), zoom && /*#__PURE__*/_react.default.createElement("div", {
    className: "ra-arch-btns-group"
  }, /*#__PURE__*/_react.default.createElement(_antd.Tooltip, {
    title: "\u7F29\u5C0F"
  }, /*#__PURE__*/_react.default.createElement(_antd.Button, {
    onClick: () => onActions('smaller')
  }, /*#__PURE__*/_react.default.createElement(_Icon.default, {
    type: "icon-smaller"
  }))), /*#__PURE__*/_react.default.createElement(_antd.Tooltip, {
    title: "\u653E\u5927"
  }, /*#__PURE__*/_react.default.createElement(_antd.Button, {
    onClick: () => onActions('bigger')
  }, /*#__PURE__*/_react.default.createElement(_Icon.default, {
    type: "icon-bigger"
  }))), isFullScreen ? /*#__PURE__*/_react.default.createElement(_antd.Tooltip, {
    title: "\u8FD8\u539F"
  }, /*#__PURE__*/_react.default.createElement(_antd.Button, {
    onClick: () => onActions('normal')
  }, /*#__PURE__*/_react.default.createElement(_Icon.default, {
    type: "icon-small"
  }))) : /*#__PURE__*/_react.default.createElement(_antd.Tooltip, {
    title: "\u5168\u5C4F"
  }, /*#__PURE__*/_react.default.createElement(_antd.Button, {
    onClick: () => onActions('full')
  }, /*#__PURE__*/_react.default.createElement(_Icon.default, {
    type: "icon-full"
  })))), plusActions && /*#__PURE__*/_react.default.createElement("div", {
    className: "ra-arch-btns-group"
  }, importData && /*#__PURE__*/_react.default.createElement(_antd.Tooltip, {
    title: "\u672C\u5730\u5BFC\u5165\u6570\u636E"
  }, /*#__PURE__*/_react.default.createElement(_antd.Button, {
    onClick: () => onActions('importData')
  }, /*#__PURE__*/_react.default.createElement(_Icon.default, {
    type: "icon-import-data"
  }))), exportData && /*#__PURE__*/_react.default.createElement(_antd.Tooltip, {
    title: "\u672C\u5730\u5BFC\u51FA\u6570\u636E"
  }, /*#__PURE__*/_react.default.createElement(_antd.Button, {
    onClick: () => onActions('exportData')
  }, /*#__PURE__*/_react.default.createElement(_Icon.default, {
    type: "icon-export-data"
  }))), exportImg && /*#__PURE__*/_react.default.createElement(_antd.Tooltip, {
    title: "\u672C\u5730\u5BFC\u51FA\u56FE\u7247"
  }, /*#__PURE__*/_react.default.createElement(_antd.Button, {
    onClick: () => onActions('exportImg')
  }, /*#__PURE__*/_react.default.createElement(_Icon.default, {
    type: "icon-export-img"
  }))), submit && /*#__PURE__*/_react.default.createElement(_antd.Tooltip, {
    title: "\u4E0A\u4F20\u6570\u636E"
  }, /*#__PURE__*/_react.default.createElement(_antd.Button, {
    onClick: () => onActions('save')
  }, /*#__PURE__*/_react.default.createElement(_Icon.default, {
    type: "icon-save-clound"
  }))), copy && /*#__PURE__*/_react.default.createElement(_antd.Tooltip, {
    title: "\u590D\u5236\u8282\u70B9"
  }, /*#__PURE__*/_react.default.createElement(_antd.Button, {
    onClick: () => onActions('copy')
  }, /*#__PURE__*/_react.default.createElement(_Icon.default, {
    type: "icon-copy"
  }))), paste && /*#__PURE__*/_react.default.createElement(_antd.Tooltip, {
    title: "\u7C98\u8D34\u8282\u70B9"
  }, /*#__PURE__*/_react.default.createElement(_antd.Button, {
    onClick: () => onActions('paste')
  }, /*#__PURE__*/_react.default.createElement(_Icon.default, {
    type: "icon-paste"
  }))), del && /*#__PURE__*/_react.default.createElement(_antd.Tooltip, {
    title: "\u5220\u9664\u8282\u70B9"
  }, /*#__PURE__*/_react.default.createElement(_antd.Button, {
    onClick: () => onActions('delete')
  }, /*#__PURE__*/_react.default.createElement(_Icon.default, {
    type: "icon-delete"
  }))), clear && /*#__PURE__*/_react.default.createElement(_antd.Tooltip, {
    title: "\u6E05\u7406\u7F13\u5B58"
  }, /*#__PURE__*/_react.default.createElement(_antd.Button, {
    onClick: () => onActions('clearStorage')
  }, /*#__PURE__*/_react.default.createElement(_Icon.default, {
    type: "icon-clear-storage"
  }))))), /*#__PURE__*/_react.default.createElement("div", {
    className: "ra-arch-systemDesc"
  }, appendActions));
};

var _default = ActionButtons;
exports.default = _default;