"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _react = _interopRequireWildcard(require("react"));

var _ahooks = require("ahooks");

var _x = require("@antv/x6");

var _tools = require("./tools");

function _getRequireWildcardCache(nodeInterop) { if (typeof WeakMap !== "function") return null; var cacheBabelInterop = new WeakMap(); var cacheNodeInterop = new WeakMap(); return (_getRequireWildcardCache = function _getRequireWildcardCache(nodeInterop) { return nodeInterop ? cacheNodeInterop : cacheBabelInterop; })(nodeInterop); }

function _interopRequireWildcard(obj, nodeInterop) { if (!nodeInterop && obj && obj.__esModule) { return obj; } if (obj === null || typeof obj !== "object" && typeof obj !== "function") { return { default: obj }; } var cache = _getRequireWildcardCache(nodeInterop); if (cache && cache.has(obj)) { return cache.get(obj); } var newObj = {}; var hasPropertyDescriptor = Object.defineProperty && Object.getOwnPropertyDescriptor; for (var key in obj) { if (key !== "default" && Object.prototype.hasOwnProperty.call(obj, key)) { var desc = hasPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : null; if (desc && (desc.get || desc.set)) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } newObj.default = obj; if (cache) { cache.set(obj, newObj); } return newObj; }

var List = function List(_ref) {
  var {
    onNodeCreate,
    configs,
    graph,
    assets,
    defaultNodeShape,
    moduleNodeShape
  } = _ref;
  var listRef = (0, _react.useRef)(null);

  var initPage = (graph, configs) => {
    if (!graph || !configs || !listRef.current) return;
    var stencilConfig = (0, _tools.getConfig)(graph, configs, assets, onNodeCreate, defaultNodeShape); // #region 初始化 stencil

    var stencil = new _x.Addon.Stencil(stencilConfig);
    (0, _tools.appendOptions)(graph, stencil, configs.options, assets, moduleNodeShape);
    listRef.current.appendChild(stencil.container);
  };

  (0, _ahooks.useDebounceEffect)(() => {
    initPage(graph, configs);
  }, [graph, configs], {
    wait: 100
  });
  return /*#__PURE__*/_react.default.createElement("div", {
    className: "ra-arch-list ".concat(configs.title ? '' : 'without-title'),
    ref: listRef
  });
};

var _default = /*#__PURE__*/_react.default.memo(List);

exports.default = _default;