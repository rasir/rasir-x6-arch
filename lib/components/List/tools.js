"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.appendOptions = appendOptions;
exports.getConfig = getConfig;

var _tools = require("../Lane/tools");

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); enumerableOnly && (symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; })), keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = null != arguments[i] ? arguments[i] : {}; i % 2 ? ownKeys(Object(source), !0).forEach(function (key) { _defineProperty(target, key, source[key]); }) : Object.getOwnPropertyDescriptors ? Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)) : ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function getConfig(target, configs, assets, onNodeCreate, defaultNodeShape) {
  var {
    title,
    search,
    options
  } = configs;
  return {
    title,
    target,
    animation: false,
    validateNode: node => {
      var _cells$find;

      var {
        x,
        y
      } = node.position();
      var {
        width: w,
        height: h
      } = node.size();
      var [px, py] = [x + w / 2, y + h / 2];
      var cells = target.getNodesFromPoint({
        x: px,
        y: py
      });
      var preId = (_cells$find = cells.find(cell => cell.data.type === 'lane')) === null || _cells$find === void 0 ? void 0 : _cells$find.id;

      if (preId && onNodeCreate) {
        node.data.preId = preId;
        onNodeCreate(node);
      }

      return false;
    },
    getDragNode: node => {
      return target.createNode({
        shape: defaultNodeShape,
        label: node.label,
        data: node.data,
        type: 'node',
        attrs: _objectSpread({
          body: {
            fill: '#ccc'
          }
        }, (0, _tools.getNodeAttr)(defaultNodeShape, node.data, assets))
      });
    },
    search: (cell, keyword) => {
      if (keyword) {
        return cell.shape === 'moduleNode' && "".concat(cell.attr('text/text')).toLowerCase().includes("".concat(keyword).toLowerCase());
      }

      return true;
    },
    placeholder: (search === null || search === void 0 ? void 0 : search.placeholder) || '请输入组件名称',
    notFoundText: '没有匹配的模型',
    stencilGraphWidth: 200,
    collapsable: false,
    groups: options.map(_ref => {
      var {
        label,
        value,
        children
      } = _ref;
      return {
        title: label,
        name: value,
        collapsable: false,
        graphPadding: 0,
        graphHeight: (Array.isArray(children) ? children.length * 32 : 0) + 10,
        layoutOptions: {
          columns: 1,
          columnWidth: 190,
          rowHeight: 32,
          dx: 5,
          dy: 5
        }
      };
    })
  };
}

function appendOptions(graph, stencil, options, assets, moduleNodeShape) {
  var moduleProps = {};
  options.forEach(group => {
    var {
      value: groupName,
      children
    } = group;
    if (!Array.isArray(children)) return;
    var items = children.map(item => {
      var {
        label,
        value,
        moduleType,
        moduleProp
      } = item;
      moduleProps[moduleType || value] = moduleProp;
      item.moduleType = moduleType || value;
      var attrs = {};
      attrs = (0, _tools.getNodeAttr)(moduleNodeShape, item, assets);
      var node = graph.createNode({
        shape: 'moduleNode',
        label,
        data: item,
        attrs
      });
      return node;
    });
    stencil.load(items, groupName);
  });
  window._moduleProps = moduleProps;
}