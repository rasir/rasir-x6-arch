import { Graph, Node } from '@antv/x6';
export declare function getConfig(target: Graph, configs: any, assets: any, onNodeCreate: (node: Node) => void, defaultNodeShape: string): any;
export declare function appendOptions(graph: Graph, stencil: any, options: any[], assets: any, moduleNodeShape: string): void;
