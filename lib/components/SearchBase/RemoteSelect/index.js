"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _ahooks = require("ahooks");

var _antd = require("antd");

var _react = _interopRequireWildcard(require("react"));

var _excluded = ["remote", "placeholder", "value", "onChange", "onSelected"];

function _getRequireWildcardCache(nodeInterop) { if (typeof WeakMap !== "function") return null; var cacheBabelInterop = new WeakMap(); var cacheNodeInterop = new WeakMap(); return (_getRequireWildcardCache = function _getRequireWildcardCache(nodeInterop) { return nodeInterop ? cacheNodeInterop : cacheBabelInterop; })(nodeInterop); }

function _interopRequireWildcard(obj, nodeInterop) { if (!nodeInterop && obj && obj.__esModule) { return obj; } if (obj === null || typeof obj !== "object" && typeof obj !== "function") { return { default: obj }; } var cache = _getRequireWildcardCache(nodeInterop); if (cache && cache.has(obj)) { return cache.get(obj); } var newObj = {}; var hasPropertyDescriptor = Object.defineProperty && Object.getOwnPropertyDescriptor; for (var key in obj) { if (key !== "default" && Object.prototype.hasOwnProperty.call(obj, key)) { var desc = hasPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : null; if (desc && (desc.get || desc.set)) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } newObj.default = obj; if (cache) { cache.set(obj, newObj); } return newObj; }

function _extends() { _extends = Object.assign ? Object.assign.bind() : function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

function _objectWithoutProperties(source, excluded) { if (source == null) return {}; var target = _objectWithoutPropertiesLoose(source, excluded); var key, i; if (Object.getOwnPropertySymbols) { var sourceSymbolKeys = Object.getOwnPropertySymbols(source); for (i = 0; i < sourceSymbolKeys.length; i++) { key = sourceSymbolKeys[i]; if (excluded.indexOf(key) >= 0) continue; if (!Object.prototype.propertyIsEnumerable.call(source, key)) continue; target[key] = source[key]; } } return target; }

function _objectWithoutPropertiesLoose(source, excluded) { if (source == null) return {}; var target = {}; var sourceKeys = Object.keys(source); var key, i; for (i = 0; i < sourceKeys.length; i++) { key = sourceKeys[i]; if (excluded.indexOf(key) >= 0) continue; target[key] = source[key]; } return target; }

var {
  Option
} = _antd.Select;

var RemoteSelect = function RemoteSelect(_ref) {
  var {
    remote,
    placeholder,
    value,
    onChange,
    onSelected
  } = _ref,
      props = _objectWithoutProperties(_ref, _excluded);

  var [state, setState] = (0, _react.useState)();
  var [options, setOptions] = (0, _react.useState)([]);
  (0, _react.useEffect)(() => {
    if (value !== state) setState(value);
  }, [value]);

  var changeValue = v => {
    if (onChange) onChange(v);else setState(v);
    onSelected && onSelected(options.find(item => item.value === v));
  };

  var {
    run: debounceFetcher
  } = (0, _ahooks.useDebounceFn)( /*#__PURE__*/function () {
    var _ref2 = _asyncToGenerator(function* (value) {
      if (remote) {
        var list = yield remote(value);
        setOptions(list || []);
      } else {
        setOptions([]);
      }
    });

    return function (_x) {
      return _ref2.apply(this, arguments);
    };
  }(), {
    wait: 300
  });
  return /*#__PURE__*/_react.default.createElement("div", null, /*#__PURE__*/_react.default.createElement(_antd.Select, _extends({}, props, {
    placeholder: placeholder,
    showSearch: true,
    filterOption: false,
    value: state,
    onChange: changeValue,
    onSearch: debounceFetcher
  }), options.map(opt => /*#__PURE__*/_react.default.createElement(Option, {
    key: opt.value,
    value: opt.value
  }, opt.label))));
};

var _default = RemoteSelect;
exports.default = _default;