"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = exports.BaseItem = void 0;

var _react = _interopRequireDefault(require("react"));

var _antd = require("antd");

var _RemoteSelect = _interopRequireDefault(require("./RemoteSelect"));

var _excluded = ["type", "options"],
    _excluded2 = ["type", "inputProps"];

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _extends() { _extends = Object.assign ? Object.assign.bind() : function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

function _objectWithoutProperties(source, excluded) { if (source == null) return {}; var target = _objectWithoutPropertiesLoose(source, excluded); var key, i; if (Object.getOwnPropertySymbols) { var sourceSymbolKeys = Object.getOwnPropertySymbols(source); for (i = 0; i < sourceSymbolKeys.length; i++) { key = sourceSymbolKeys[i]; if (excluded.indexOf(key) >= 0) continue; if (!Object.prototype.propertyIsEnumerable.call(source, key)) continue; target[key] = source[key]; } } return target; }

function _objectWithoutPropertiesLoose(source, excluded) { if (source == null) return {}; var target = {}; var sourceKeys = Object.keys(source); var key, i; for (i = 0; i < sourceKeys.length; i++) { key = sourceKeys[i]; if (excluded.indexOf(key) >= 0) continue; target[key] = source[key]; } return target; }

var {
  RangePicker
} = _antd.DatePicker;

var BaseItem = function BaseItem(_ref) {
  var {
    type,
    options
  } = _ref,
      props = _objectWithoutProperties(_ref, _excluded);

  if (!props.style) props.style = {};

  if (!props.style.width) {
    props.style.width = '100%';
  }

  return /*#__PURE__*/_react.default.createElement(_react.default.Fragment, null, type === 'INPUT' && /*#__PURE__*/_react.default.createElement(_antd.Input, props), type === 'TEXTAREA' && /*#__PURE__*/_react.default.createElement(_antd.Input.TextArea, props), type === 'SEARCH' && /*#__PURE__*/_react.default.createElement(_antd.Input.Search, props), type === 'REMOTESELECT' && /*#__PURE__*/_react.default.createElement(_RemoteSelect.default, props), type === 'SELECT' && /*#__PURE__*/_react.default.createElement(_antd.Select, props, options === null || options === void 0 ? void 0 : options.map(e => /*#__PURE__*/_react.default.createElement(_antd.Select.Option, {
    key: e.value,
    value: e.value
  }, e.label))), type === 'DATETIMERANGER' && /*#__PURE__*/_react.default.createElement(RangePicker, _extends({
    showTime: true
  }, props)), type === 'DATERANGER' && /*#__PURE__*/_react.default.createElement(RangePicker, props));
};

exports.BaseItem = BaseItem;
var formLayout = {
  wrapperCol: {
    span: 16
  },
  labelCol: {
    span: 8
  }
};

var SearchBase = function SearchBase(_ref2) {
  var {
    items = [],
    form
  } = _ref2;
  return /*#__PURE__*/_react.default.createElement(_antd.Form, _extends({}, formLayout, {
    className: "ra-arch-searchBase",
    style: {
      background: '#fff'
    },
    form: form
  }), items.map((_ref3, index) => {
    var {
      type,
      inputProps
    } = _ref3,
        props = _objectWithoutProperties(_ref3, _excluded2);

    return /*#__PURE__*/_react.default.createElement(_antd.Form.Item, _extends({
      key: "".concat(index + 1)
    }, props), /*#__PURE__*/_react.default.createElement(BaseItem, _extends({
      type: type
    }, inputProps)));
  }));
};

var _default = SearchBase;
exports.default = _default;