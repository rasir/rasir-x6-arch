import React from 'react';
import { FormInstance } from 'antd/lib/form/Form';
import { FormItemProps } from 'antd/lib/form/FormItem';
export declare type ItemProps = FormItemProps & {
    type: string;
    inputProps?: any;
};
interface SearchBaseProps {
    items: ItemProps[];
    form: FormInstance;
}
export declare const BaseItem: React.FC<any>;
declare const SearchBase: React.FC<SearchBaseProps>;
export default SearchBase;
