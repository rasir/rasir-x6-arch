import { Graph, Node } from '@antv/x6';
import IndexdbHelper from './components/IndexdbHelper';
import { KeyValue, NodeData, ArchData, GraphData } from './types';
export declare const defaultModuleNodeName = "moduleNode";
export declare const defaultEdgeShapeName = "laneEdge";
export declare const dbHelper: IndexdbHelper;
export declare function autoSaveJson(graphData: ArchData | null, graph?: Graph | null, onAutoSave?: (data: any) => void): Promise<unknown>;
export declare function getLaneStyle(index: number): {
    nameRect: {
        fill: string;
    };
    nameText: {
        fill: string;
    };
    bottomBorder: {
        stroke: string;
    };
};
export declare function getGraphData(dataSource: ArchData, defaultEdgeShape: string): GraphData;
/**
 * 用于实例详情图中任务名称显示
 * @param {string} label 需要处理的字符
 * @param {number} maxTextLen 最大长度 默认20
 * @param {number} maxLine  最大行数 默认1
 * @param {string} placehoder 中间占位字符 默认...
 */
export declare function getLabelText({ label, placehoder, maxTextLen, maxLine, }: {
    label?: string | undefined;
    placehoder?: string | undefined;
    maxTextLen?: number | undefined;
    maxLine?: number | undefined;
}): string;
export declare function nextTick(callback: (...args: any) => void): Promise<void>;
export declare function getDndConfig(target: any): {
    target: any;
    validateNode: () => boolean;
    animation: boolean;
    getDragNode: () => void;
    getDropNode: () => void;
};
export declare function createGraphNodeData(nodeData: NodeData, config?: {
    graph: Graph;
    assets: KeyValue;
}): Node.Metadata;
export declare function getGraphJSONData(graph?: Graph): ArchData;
export declare function launchFullScreen(element: any): void;
export declare function exitFullscreen(): void;
export declare function getKey(): string;
