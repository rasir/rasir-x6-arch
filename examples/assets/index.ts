import appIcon from './icon/app.png';
import bmsIcon from './icon/bms.png';
import dubboIcon from './icon/dubbo.png';
import jettyIcon from './icon/jetty.png';
import kafkaIcon from './icon/kafka.png';
import mysqlIcon from './icon/mysql.png';
import nginxIcon from './icon/nginx.png';
import nodejsIcon from './icon/nodejs.png';
import otherIcon from './icon/other.png';
import pythonIcon from './icon/python.png';
import redisIcon from './icon/redis.png';
import springbootIcon from './icon/springboot.png';
import userIcon from './icon/user.png';
import zookeeperIcon from './icon/zookeeper.png';
import appModule from './module/APP.png';
import bmrModule from './module/BMR.png';
import browserModule from './module/Browser.png';
import dubboModule from './module/Dubbo.png';
import ecsModule from './module/ECS.png';
import flinkModule from './module/Flink.png';
import jettyModule from './module/Jetty.png';
import jstormModule from './module/Jstorm.png';
import k8sModule from './module/K8S.png';
import kafkaModule from './module/Kafka.png';
import mongodbModule from './module/MongoDB.png';
import mycatModule from './module/Mycat.png';
import mysqlModule from './module/Mysql.png';
import nasModule from './module/Nas.png';
import nginxModule from './module/Nginx.png';
import nodejsModule from './module/Nodejs.png';
import ossModule from './module/OSS.png';
import oracleModule from './module/Oracle.png';
import otherModule from './module/Other.png';
import pythonModule from './module/Python.png';
import redisModule from './module/Redis.png';
import sparkModule from './module/Spark.png';
import springbootModule from './module/SpringBoot.png';
import tidbModule from './module/TiDB.png';
import zookeeperModule from './module/ZooKeeper.png';
import userModule from './module/user.png';
import innerSystem from './system/inner.png';
import outerSystem from './system/outer.png';
export default {
  appIcon,
  bmsIcon,
  dubboIcon,
  jettyIcon,
  kafkaIcon,
  mysqlIcon,
  nginxIcon,
  nodejsIcon,
  otherIcon,
  pythonIcon,
  redisIcon,
  springbootIcon,
  userIcon,
  zookeeperIcon,
  appModule,
  bmrModule,
  browserModule,
  dubboModule,
  ecsModule,
  flinkModule,
  jettyModule,
  jstormModule,
  k8sModule,
  kafkaModule,
  mongodbModule,
  mycatModule,
  mysqlModule,
  nasModule,
  nginxModule,
  nodejsModule,
  ossModule,
  oracleModule,
  otherModule,
  pythonModule,
  redisModule,
  sparkModule,
  springbootModule,
  tidbModule,
  zookeeperModule,
  userModule,
  innerSystem,
  outerSystem,
};
