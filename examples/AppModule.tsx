import { Form, Input } from 'antd';
import { FormInstance } from 'antd/es/form/Form';
import React, { useEffect } from 'react';

interface AppModuleProps {
  data?: any;
  form: FormInstance;
  [key: string]: any;
}

const { Item: FormItem } = Form;

const AppModule: React.FC<AppModuleProps> = function (props) {
  const { form, data } = props;

  useEffect(() => {
    if (data) {
      form?.setFieldsValue(data);
    } else {
      form?.resetFields();
    }
  }, [data]);

  return (
    <Form form={form}>
      <FormItem
        name="name"
        label="APP名称"
        rules={[
          {
            required: true,
            message: '请填写app名称',
          },
        ]}
      >
        <Input placeholder="请填写app名称"></Input>
      </FormItem>
    </Form>
  );
};

export default AppModule;
