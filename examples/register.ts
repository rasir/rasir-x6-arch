import { Register } from '../es/types';
import { getLabelText } from '../src/tools';

const register: Register[] = [
  {
    name: 'node',
    type: 'NODE',
    handler: (data, assets) => {
      const { moduleType, systemType, name } = data;
      return {
        text: {
          text: getLabelText({
            label: name,
            maxTextLen: 20,
            maxLine: 2,
            placehoder: '...',
          }),
        },
        itemTag: {
          xlinkHref: assets[`${(moduleType || '').toLowerCase()}Module`],
        },
        backgorund: {
          xlinkHref: assets[`${(systemType || '').toLowerCase()}System`],
        },
      };
    },
    options: {
      inherit: 'rect',
      width: 100,
      height: 100,
      markup: [
        {
          tagName: 'rect',
          selector: 'body',
        },
        {
          tagName: 'text',
          selector: 'text',
        },
        {
          tagName: 'image',
          selector: 'backgorund',
          className: 'mainImage',
        },
        {
          tagName: 'image',
          selector: 'itemTag',
        },
      ],
      attrs: {
        body: {
          strokeWidth: 0,
          // stroke: 'transparent',
          fill: 'transparent',
        },
        text: {
          fontSize: 10,
          fill: '#262626',
          refY: '80%',
        },
        backgorund: {
          refX: '10%',
          refY: '10%',
          refWidth: '80%',
          refHeight: '50%',
          opacity: 1,
        },
        itemTag: {
          ref: 'backgorund',
          refX: '35%',
          refY: '15%',
          refWidth: '30%',
          refHeight: '30%',
          opacity: 1,
        },
      },
      ports: {
        groups: {
          top: {
            position: 'top',
            attrs: {
              circle: {
                r: 4,
                magnet: true,
                stroke: '#5F95FF',
                strokeWidth: 1,
                fill: '#fff',
                style: {
                  visibility: 'hidden',
                },
              },
            },
          },
          right: {
            position: 'right',
            attrs: {
              circle: {
                r: 4,
                magnet: true,
                stroke: '#5F95FF',
                strokeWidth: 1,
                fill: '#fff',
                style: {
                  visibility: 'hidden',
                },
              },
            },
          },
          bottom: {
            position: 'bottom',
            attrs: {
              circle: {
                r: 4,
                magnet: true,
                stroke: '#5F95FF',
                strokeWidth: 1,
                fill: '#fff',
                style: {
                  visibility: 'hidden',
                },
              },
            },
          },
          left: {
            position: 'left',
            attrs: {
              circle: {
                r: 4,
                magnet: true,
                stroke: '#5F95FF',
                strokeWidth: 1,
                fill: '#fff',
                style: {
                  visibility: 'hidden',
                },
              },
            },
          },
        },
        items: [
          {
            group: 'top',
          },
          {
            group: 'right',
          },
          {
            group: 'bottom',
          },
          {
            group: 'left',
          },
        ],
      },
    },
  },
  {
    name: 'link',
    type: 'EDGE',
    options: {
      inherit: 'edge',
      attrs: {
        line: {
          stroke: '#f00',
          strokeWidth: 10,
        },
      },
      label: {
        attrs: {
          label: {
            fill: '#A2B1C3',
            fontSize: 12,
          },
        },
      },
    },
  },
  {
    name: 'arch-node',
    type: 'NODE',
    handler: (data, assets) => {
      const { moduleType, name } = data;
      return {
        text: {
          text: getLabelText({
            label: name,
            maxTextLen: 20,
            maxLine: 2,
            placehoder: '...',
          }),
        },
        itemTag: {
          xlinkHref: assets[`${(moduleType || '').toLowerCase()}Module`],
        },
      };
    },
    options: {
      inherit: 'rect',
      width: 100,
      height: 100,
      markup: [
        {
          tagName: 'rect',
          selector: 'body',
        },
        {
          tagName: 'text',
          selector: 'text',
        },
        {
          tagName: 'image',
          selector: 'backgorund',
          className: 'mainImage',
        },
        {
          tagName: 'image',
          selector: 'itemTag',
        },
      ],
      attrs: {
        body: {
          strokeWidth: 0,
          // stroke: 'transparent',
          fill: 'transparent',
        },
        text: {
          fontSize: 12,
          fill: '#262626',
          refY: '80%',
        },
        backgorund: {
          refX: '10%',
          refY: '10%',
          refWidth: '80%',
          refHeight: '50%',
          opacity: 0,
        },
        itemTag: {
          ref: 'backgorund',
          refX: '15%',
          refY: '15%',
          refWidth: '80%',
          refHeight: '80%',
          opacity: 1,
        },
      },
      ports: {
        groups: {
          top: {
            position: 'top',
            attrs: {
              circle: {
                r: 4,
                magnet: true,
                stroke: '#5F95FF',
                strokeWidth: 1,
                fill: '#fff',
                style: {
                  visibility: 'hidden',
                },
              },
            },
          },
          right: {
            position: 'right',
            attrs: {
              circle: {
                r: 4,
                magnet: true,
                stroke: '#5F95FF',
                strokeWidth: 1,
                fill: '#fff',
                style: {
                  visibility: 'hidden',
                },
              },
            },
          },
          bottom: {
            position: 'bottom',
            attrs: {
              circle: {
                r: 4,
                magnet: true,
                stroke: '#5F95FF',
                strokeWidth: 1,
                fill: '#fff',
                style: {
                  visibility: 'hidden',
                },
              },
            },
          },
          left: {
            position: 'left',
            attrs: {
              circle: {
                r: 4,
                magnet: true,
                stroke: '#5F95FF',
                strokeWidth: 1,
                fill: '#fff',
                style: {
                  visibility: 'hidden',
                },
              },
            },
          },
        },
        items: [
          {
            group: 'top',
          },
          {
            group: 'right',
          },
          {
            group: 'bottom',
          },
          {
            group: 'left',
          },
        ],
      },
    },
  },
];

export default register;
