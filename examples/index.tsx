import React, { useEffect, useRef, useState } from 'react';
import ReactDOM from 'react-dom';
import data from './demoData.json';
import moduleConfig from './moduleConfig';
import assets from './assets';
import Arch from '../src';
import registerList from './register';
import 'antd/dist/antd.css';
import './index.less';

const App = () => {
  const ref = useRef();

  const onCreateNode = (nodeData) => {
    const { name, preId, moduleType } = nodeData;
    return Promise.resolve({
      id: '100',
      name,
      shape: 'arch-node',
      type: 'node',
      preId,
      moduleType,
    });
  };

  const addResize = () => {
    function resize() {
      (ref.current as any).resize();
    }
    window.addEventListener('resize', resize);
    return function () {
      window.removeEventListener('resize', resize);
    };
  };

  useEffect(() => {
    const unListener = addResize();
    return () => {
      unListener();
    };
  }, []);

  return (
    <>
      <Arch
        data={data}
        ref={ref}
        moduleConfig={moduleConfig}
        assets={assets}
        appendActions={<div>hello</div>}
        registerList={registerList}
        defaultNodeShape="arch-node"
        defaultEdgeShape="link"
        onCreateNode={onCreateNode}
        cache={false}
        onChange={(data) => {}}
      />
    </>
  );
};

ReactDOM.render(<App />, document.getElementById('root'));
